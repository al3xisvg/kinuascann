use zinoutvg$HZ;

delimiter //

CREATE PROCEDURE `sp_historial_insertar`(
	in pVegetacion double,
    in pAmarilleamiento double,
    in pUsername varchar(100),
    in pSurco varchar(45),
    in pCultivo varchar(45),
    in pFecCreated datetime
)
BEGIN

	INSERT INTO HISTORIAL(Vegetacion,Amarilleamiento,Username,Surco,Cultivo,FecCreated) 
    VALUES(pVegetacion, pAmarilleamiento, pUsername, pSurco, pCultivo, pFecCreated);

END

// 

delimiter ;