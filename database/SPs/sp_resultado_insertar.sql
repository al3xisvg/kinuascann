use zinoutvg$HZ;

delimiter //

CREATE PROCEDURE `sp_resultado_insertar`(
	in pVegetacion double,
    in pAmarilleamiento double,
    
    in pidUsuario int,
    in pidSurco int,
    in pidCultivo int
)
BEGIN

	-- Validacion de Errores --
	declare _error int default 0;	
    loop_err: BEGIN
		set @exists_user := 0;
        set @exists_surco := 0;
        set @nSurcos := 0;
        set @nSurcosOficial := 0;
        select count(*) into @nSurcosOficial from SURCOS where Cultivo_id=pidCultivo;
        
		select count(*) into @exists_user from USUARIOS where idUsuario=pidUsuario;
        if @exists_user < 1 then set _error := 301; leave loop_err; end if;
        
        select count(*) into @exists_surco from SURCOS where idSurco=pidSurco;
        if @exists_surco < 1 then set _error := 302; leave loop_err; end if;
        
        select count(*) into @nSurcos from REGISTROS rg
		inner join RESULTADOS rs on rg.Resultado_id = rs.idResultado
		inner join SURCOS s on rs.Surco_id = s.idSurco
        inner join USUARIOS u on rg.Usuario_id = u.idUsuario
		where s.Cultivo_id=1 and rg.FecCreated < now() and (select concat(DATE(now()),' 00:00:00'));
        if @nSurcos > (@nSurcosOficial-1) then set _error := 400; leave loop_err; end if;
    END loop_err;

	-- Procedimiento --
    if _error = 0 then

		SET @id_resultado := 0;
		
		INSERT INTO RESULTADOS (Vegetacion,Amarilleamiento,Surco_id) 
		VALUES (pVegetacion,pAmarilleamiento,pidSurco);
		
		SELECT LAST_INSERT_ID() into @id_resultado;
		
		INSERT INTO REGISTROS (FecCreated,Resultado_id,Usuario_id)
		VALUES (now(),@id_resultado,pidUsuario);

		SELECT 	rg.idRegistro,
				rs.Vegetacion,
				rs.Amarilleamiento,
				rg.FecCreated,
				rg.FecModified,
				rg.Usuario_id,
                rs.Surco_id,
				s.Cultivo_id

		FROM REGISTROS rg
		INNER JOIN RESULTADOS rs on rg.Resultado_id = rs.idResultado
        INNER JOIN SURCOS s on rs.Surco_id = s.idSurco
		WHERE rg.Resultado_id = @id_resultado;

	else 
		SELECT 
			'True'			AS 'Error',
            Codigo			AS 'Error_Code',
            Tipo			AS 'Error_Type',
            Descripcion		AS 'Error_Description'
		FROM ERRORES
        WHERE Codigo = _error;
    end if;

END

//

delimiter ;