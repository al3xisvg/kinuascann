use zinoutvg$HZ;

delimiter //

CREATE PROCEDURE `sp_usuario_insertar`(
    in pNombre varchar(45),
    in pPaterno varchar(45),
    in pMaterno varchar(45),
    in pDNI varchar(45),
    in pFecNac date,
    in pEmail varchar(100),
    
    in pLocalidad_id int,
    
    in pUsername varchar(100),
    in pContrasena varchar(200),
    in pRol_id int,
    in pEstado_id int
)
BEGIN

    -- Validacion de Errores --
    declare _error int default 0;   
    loop_err: BEGIN 
        set @exists_user := 0;
        select count(*) into @exists_user from USUARIOS where Username=pUsername;
        if @exists_user > 0 then set _error := 100; leave loop_err; end if;
        select count(*) into @exists_user from PERSONAS where DNI=pDNI;
        if @exists_user > 0 then set _error := 101; leave loop_err; end if;
        select count(*) into @exists_user from PERSONAS where Email=pEmail;
        if @exists_user > 0 then set _error := 102; leave loop_err; end if;
    END loop_err;
    
    -- Procedimiento --
    if _error = 0 then
        
        SET @id_persona := 0;
        
        INSERT INTO PERSONAS(Nombre,Paterno,Materno,DNI,FecNac,Email,FecCreated, Localidad_id) 
        VALUES (pNombre,pPaterno,pMaterno,pDNI,pFecNac,pEmail,now(),pLocalidad_id);
        
        SELECT LAST_INSERT_ID() into @id_persona;
        
        INSERT INTO USUARIOS(Username,Contrasena,FecCreated,Rol_id,Estado_id,Persona_id)
        VALUES (pUsername,pContrasena,now(),pRol_id,pEstado_id,@id_persona);

        SELECT  u.idUsuario,
                u.Username,
                u.FecCreated,
                u.FecModified,
                u.Rol_id,
                u.Estado_id,
                
                p.Nombre,
                p.Paterno,
                p.Materno,
                p.DNI,
                p.FecNac,
                p.Email,
                
                p.Localidad_id
        
        FROM USUARIOS u
        INNER JOIN PERSONAS p on u.Persona_id=p.idPersona
        WHERE u.Persona_id = @id_persona;
    
    else 
        SELECT 
            'True'          AS 'Error',
            Codigo          AS 'Error_Code',
            Tipo            AS 'Error_Type',
            Descripcion     AS 'Error_Description'
        FROM ERRORES
        WHERE Codigo = _error;
    end if;

END

// 

delimiter ; 