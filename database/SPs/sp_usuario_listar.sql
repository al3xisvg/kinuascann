use zinoutvg$HZ;

delimiter // 

CREATE PROCEDURE `sp_usuario_listar`(
	in pCodEstado varchar(45)
)
BEGIN
	-- Validacion de Errores --
    declare _error int default 0;	
    loop_err: BEGIN 
		if pCodEstado='ALL' then set _error := 0; leave loop_err; end if;
        
        set @exists_estado := 0;
        
        select count(*) into @exists_estado from ESTADOS where Codigo=pCodEstado;
        if @exists_estado < 1 then set _error := 300; leave loop_err; end if;
        
    END loop_err;
    
    -- Procedimiento --
    if _error = 0 then
    
		if pCodEstado='ALL' then
			SELECT  u.idUsuario,
					u.Username,
					u.FecCreated,
					u.FecModified,
					u.Rol_id,
					u.Estado_id,
					
					p.Nombre,
					p.Paterno,
					p.Materno,
					p.DNI,
					p.FecNac,
					p.Email,
					
					p.Localidad_id
			
			FROM USUARIOS u
			INNER JOIN PERSONAS p on u.Persona_id=p.idPersona
			INNER JOIN ESTADOS e on u.Estado_id=e.idEstado;
		else
			SELECT  u.idUsuario,
					u.Username,
					u.FecCreated,
					u.FecModified,
					u.Rol_id,
					u.Estado_id,
					
					p.Nombre,
					p.Paterno,
					p.Materno,
					p.DNI,
					p.FecNac,
					p.Email,
					
					p.Localidad_id
			
			FROM USUARIOS u
			INNER JOIN PERSONAS p on u.Persona_id=p.idPersona
			INNER JOIN ESTADOS e on u.Estado_id=e.idEstado
			WHERE e.Codigo=pCodEstado;
        end if;
        
	else 
		SELECT 
			'True'			AS 'Error',
            Codigo			AS 'Error_Code',
            Tipo			AS 'Error_Type',
            Descripcion		AS 'Error_Description'
		FROM ERRORES
        WHERE Codigo = _error;
    end if;

END

//

delimiter ;