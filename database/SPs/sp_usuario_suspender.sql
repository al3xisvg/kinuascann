use zinoutvg$HZ;

delimiter //

CREATE PROCEDURE `sp_usuario_suspender`(
	in pidUsuario int
)
BEGIN

	-- Validacion de Errores --
    declare _error int default 0;	
    loop_err: BEGIN 
		set @exists_user := 0;
        set @id_estado := 0;
        
        select count(*) into @exists_user from USUARIOS where idUsuario=pidUsuario; 
		if @exists_user < 1 then set _error := 301; leave loop_err; end if;
        
        select idEstado into @id_estado from ESTADOS where Codigo='I';
        select count(*) into @exists_user from USUARIOS where idUsuario=pidUsuario and Estado_id=@id_estado;
        if @exists_user > 0 then set _error := 103; leave loop_err; end if;
        
    END loop_err;
    
    -- Procedimiento --
	if _error = 0 then
		
        SET @id_estado := 0;
        SELECT idEstado INTO @id_estado FROM ESTADOS where Codigo='I';
        UPDATE USUARIOS SET Estado_id=@id_estado where idUsuario=pidUsuario;
        
    else 
		SELECT 
			'True'			AS 'Error',
            Codigo			AS 'Error_Code',
            Tipo			AS 'Error_Type',
            Descripcion		AS 'Error_Description'
		FROM ERRORES
        WHERE Codigo = _error;
    end if;
    
END

//

delimiter ; 