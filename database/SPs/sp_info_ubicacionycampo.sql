use zinoutvg$HZ;

delimiter //

CREATE PROCEDURE `sp_info_ubicacionycampo`()
BEGIN
	SELECT  p.idProvincia as p_idProvincia,
            p.Codigo as p_Codigo,
            p.Nombre as p_Nombre,
            p.Descripcion as p_Descripcion,

            d.idDistrito as d_idDistrito,
            d.Codigo as d_Codigo,
            d.Nombre as d_Nombre,
            d.Descripcion as d_Descripcion,

            l.idLocalidad as l_idLocalidad,
            l.Nombre as l_Nombre,
            l.Ubicacion as l_Ubicacion,
            l.Descripcion as l_Descripcion,

            c.idCultivo as c_idCultivo,
            c.Codigo as c_Codigo,
            c.Ancho as c_Ancho,
            c.Largo as c_Largo,
            c.Perimetro as c_Perimetro,
            c.Institucion as c_Institucion,
            c.Descripcion as c_Descripcion,

            s.idSurco as s_idSurco,
			s.Codigo as s_Codigo,
            s.Longitud as s_Longitud
            
	FROM PROVINCIAS p
    INNER JOIN DISTRITOS d on d.Provincia_id = p.idProvincia
    INNER JOIN LOCALIDADES l on l.Distrito_id = d.idDistrito
    INNER JOIN CULTIVOS c on c.Localidad_id = l.idLocalidad
    INNER JOIN SURCOS s on s.Cultivo_id = c.idCultivo
    INNER JOIN ESTADOS e on c.Estado_id = e.idEstado
    WHERE e.Codigo = 'A';
END

// 

delimiter ;