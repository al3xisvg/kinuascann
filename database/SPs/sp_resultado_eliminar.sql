use zinoutvg$HZ;

delimiter //

CREATE PROCEDURE `sp_resultado_eliminar`(
	in pIds_Resultados TEXT
)
BEGIN
	
    -- Validacion de Errores --
    declare _error int default 0;   
    loop_err: BEGIN
		set @validation_str := 0;
        set @commaLess := replace(pIds_Resultados,',','');
		select @commaLess regexp '^[0-9]+$' into @validation_str;
        
        if @validation_str = 0 then set _error := 401; leave loop_err; end if;
    END loop_err;
    
    -- Procedimiento --
    if _error = 0 then
    
		SET @id_res := NULL;
		SET @cont := 1;
		SET @temp := NULL;
    
		REPEAT
			SET @id_res := substring_index(substring_index(pIds_Resultados,',',@cont),',',-1);
			SET @cont = @cont + 1;
			SET @temp := substring_index(substring_index(pIds_Resultados,',',@cont),',',-1);
			
			SET @query_rg := concat('DELETE FROM REGISTROS WHERE Resultado_id=',@id_res);
			PREPARE stmt from @query_rg;
			EXECUTE stmt;
			DEALLOCATE PREPARE stmt;
			
			SET @query_rs := concat('DELETE FROM RESULTADOS WHERE idResultado=',@id_res);
			PREPARE stmt from @query_rs;
			EXECUTE stmt;
			DEALLOCATE PREPARE stmt;
			
			UNTIL @id_res = @temp
		END REPEAT;

	else 
        SELECT 
            'True'          AS 'Error',
            Codigo          AS 'Error_Code',
            Tipo            AS 'Error_Type',
            Descripcion     AS 'Error_Description'
        FROM ERRORES
        WHERE Codigo = _error;
    end if;

END

// 

delimiter ;