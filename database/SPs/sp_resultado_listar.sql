use zinoutvg$HZ;

delimiter //

CREATE PROCEDURE `sp_resultado_listar`(
    in pFecIni datetime,
    in pFecFin datetime,
    
    in pidUsuario int,
    in pidCultivo int,
    
    in __START int,
    in __RANGE int
)
BEGIN

    -- Validacion de Errores --
    declare _error int default 0;   
    loop_err: BEGIN
        set @exists_user := 0;
        set @exists_cultivo := 0;
        
        if pidUsuario=-1 then 
            if pidCultivo=-1 then
                set _error := 0; leave loop_err;
            else
                select count(*) into @exists_cultivo from CULTIVOS where idCultivo=pidCultivo;
                if @exists_cultivo < 1 then set _error := 303; leave loop_err; end if;
            end if;
        else    
            if pidCultivo=-1 then               
                select count(*) into @exists_user from USUARIOS where idUsuario=pidUsuario;
                if @exists_user < 1 then set _error := 301; leave loop_err; end if;
            else 
                select count(*) into @exists_cultivo from CULTIVOS where idCultivo=pidCultivo;
                if @exists_cultivo < 1 then set _error := 303; leave loop_err; end if;
                select count(*) into @exists_user from USUARIOS where idUsuario=pidUsuario;
                if @exists_user < 1 then set _error := 301; leave loop_err; end if;
            end if;
        end if;
    END loop_err;
    
    -- Procedimiento --
    if _error = 0 then
    
        if pidUsuario = -1 then
            if pidCultivo = -1 then
                SELECT  rg.idRegistro,
                        rs.Vegetacion,
                        rs.Amarilleamiento,
                        rg.FecCreated,
                        rg.FecModified,
                        rg.Usuario_id,
                        rs.Surco_id,
                        s.Cultivo_id

                FROM REGISTROS rg
                INNER JOIN RESULTADOS rs on rg.Resultado_id = rs.idResultado
                INNER JOIN SURCOS s on rs.Surco_id = s.idSurco
                WHERE rg.FecCreated BETWEEN pFecIni AND pFecFin
                LIMIT __START, __RANGE;
            else
                SELECT  rg.idRegistro,
                        rs.Vegetacion,
                        rs.Amarilleamiento,
                        rg.FecCreated,
                        rg.FecModified,
                        rg.Usuario_id,
                        rs.Surco_id,
                        s.Cultivo_id

                FROM REGISTROS rg
                INNER JOIN RESULTADOS rs on rg.Resultado_id = rs.idResultado
                INNER JOIN SURCOS s on rs.Surco_id = s.idSurco
                WHERE s.Cultivo_id = pidCultivo
                AND rg.FecCreated BETWEEN pFecIni AND pFecFin
                LIMIT __START, __RANGE;
            end if;
        else
            if pidCultivo = -1 then
                SELECT  rg.idRegistro,
                        rs.Vegetacion,
                        rs.Amarilleamiento,
                        rg.FecCreated,
                        rg.FecModified,
                        rg.Usuario_id,
                        rs.Surco_id,
                        s.Cultivo_id

                FROM REGISTROS rg
                INNER JOIN RESULTADOS rs on rg.Resultado_id = rs.idResultado
                INNER JOIN SURCOS s on rs.Surco_id = s.idSurco
                WHERE rg.Usuario_id = pidUsuario
                AND rg.FecCreated BETWEEN pFecIni AND pFecFin
                LIMIT __START, __RANGE;
            else
                SELECT  rg.idRegistro,
                        rs.Vegetacion,
                        rs.Amarilleamiento,
                        rg.FecCreated,
                        rg.FecModified,
                        rg.Usuario_id,
                        rs.Surco_id,
                        s.Cultivo_id

                FROM REGISTROS rg
                INNER JOIN RESULTADOS rs on rg.Resultado_id = rs.idResultado
                INNER JOIN SURCOS s on rs.Surco_id = s.idSurco
                WHERE rg.Usuario_id = pidUsuario
                AND s.Cultivo_id = pidCultivo
                AND rg.FecCreated BETWEEN pFecIni AND pFecFin
                LIMIT __START, __RANGE;
            end if;
        end if;
        
    else 
        SELECT 
            'True'          AS 'Error',
            Codigo          AS 'Error_Code',
            Tipo            AS 'Error_Type',
            Descripcion     AS 'Error_Description'
        FROM ERRORES
        WHERE Codigo = _error;
    end if;

END

// 

delimiter ;