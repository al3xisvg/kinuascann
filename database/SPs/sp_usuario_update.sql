use zinoutvg$HZ;

delimiter //

CREATE PROCEDURE `sp_usuario_update`(
	in pidUsuario int,

	in pNombre varchar(45),
    in pPaterno varchar(45),
    in pMaterno varchar(45),
    in pDNI varchar(45),
    in pFecNac date,
    in pEmail varchar(100),
    
    in pLocalidad_id int,
    
    in pUsername varchar(100),
    in pContrasena varchar(200),
    in pRol_id int
)
BEGIN
	-- Validacion de Errores --
	declare _error int default 0;	
    loop_err: BEGIN
		set @exists_user := 0;
        set @id_persona := 0;
        select Persona_id into @id_persona from USUARIOS where idUsuario=pidUsuario;
        
		select count(*) into @exists_user from USUARIOS where idUsuario=pidUsuario; 
		if @exists_user < 1 then set _error := 301; leave loop_err; end if;
        
        select count(*) into @exists_user from USUARIOS where Username=pUsername and not idUsuario=pidUsuario;
        if @exists_user > 0 then set _error := 100; leave loop_err; end if;
        
        select count(*) into @exists_user from PERSONAS where DNI=pDNI and not idPersona=@id_persona;
        if @exists_user > 0 then set _error := 101; leave loop_err; end if;
        
        select count(*) into @exists_user from PERSONAS where Email=pEmail and not idPersona=@id_persona;
        if @exists_user > 0 then set _error := 102; leave loop_err; end if;
    END loop_err;
    
    -- Procedimiento --
    if _error = 0 then 
    
		UPDATE USUARIOS SET
		Username = pUsername,
		Contrasena = pContrasena,
		Rol_id = pRol_id,
        FecModified = now()
		WHERE idUsuario = pidUsuario;
    
		SET @id_persona := 0;
        SELECT Persona_id INTO @id_persona FROM USUARIOS WHERE idUsuario=pidUsuario;
		UPDATE PERSONAS SET
        Nombre = pNombre,
        Paterno = pPaterno,
		Materno = pMaterno,
		DNI = pDNI,
		FecNac = pFecNac,
		Email = pEmail,
        Localidad_id = pLocalidad_id,
        FecModified = now()
        WHERE idPersona = @id_persona;
    
		SELECT  u.idUsuario,
				u.Username,
				u.FecCreated,
				u.FecModified,
				u.Rol_id,
				u.Estado_id,
				
				p.Nombre,
				p.Paterno,
				p.Materno,
				p.DNI,
				p.FecNac,
				p.Email,
				
				p.Localidad_id
		
		FROM USUARIOS u
		INNER JOIN PERSONAS p on u.Persona_id=p.idPersona
        WHERE u.Persona_id = @id_persona;
		
    else 
		SELECT 
			'True'			AS 'Error',
            Codigo			AS 'Error_Code',
            Tipo			AS 'Error_Type',
            Descripcion		AS 'Error_Description'
		FROM ERRORES
        WHERE Codigo = _error;
    end if;

END

//

delimiter ; 