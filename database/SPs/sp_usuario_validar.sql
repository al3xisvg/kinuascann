use zinoutvg$HZ;

delimiter //

CREATE PROCEDURE `sp_usuario_validar`(
    in pUsername varchar(100),
    in pContrasena varchar(200)
)
BEGIN

    -- Validacion de Errores --
    declare _error int default 0;   
    loop_err: BEGIN 
        set @exists_user := 0;
        select count(*) into @exists_user from USUARIOS where Username=pUsername; 
        if @exists_user < 1 then set _error := 301; leave loop_err; end if;
        
        set @id_status := 0;
        select idEstado into @id_status from ESTADOS where Codigo='A';
        
        set @active_user := 0;
        select count(*) into @active_user from USUARIOS where Username=pUsername and Estado_id=@id_status;
        if @active_user < 1 then set _error := 402; leave loop_err; end if;

        set @correct_pwd := 0;
        select count(*) into @correct_pwd from USUARIOS where Username=pUsername and Contrasena=pContrasena;
        if @correct_pwd < 1 then set _error := 304; leave loop_err; end if;
    END loop_err;
    
    -- Procedimiento --
    if _error = 0 then
        
        SET @validate_user := 0;
        SELECT COUNT(*) INTO @validate_user FROM USUARIOS WHERE Username=pUsername AND Contrasena=pContrasena;
        
        IF @validate_user = 1 THEN
            SELECT TRUE AS Output;
        ELSE
            SELECT FALSE AS Output;
        END IF;
        
    else 
        SELECT 
            'True'          AS 'Error',
            Codigo          AS 'Error_Code',
            Tipo            AS 'Error_Type',
            Descripcion     AS 'Error_Description'
        FROM ERRORES
        WHERE Codigo = _error;
    end if;

END

//

delimiter ;