use zinoutvg$HZ;

delimiter //

CREATE PROCEDURE `sp_logs_insertar`(
	in pUsername varchar(100),
    in pEndpoint varchar(200),
    in pIP varchar(100),
    in pMetodo varchar(45),
    in pRequest varchar(300),
    in pDescripcion text,
    in pResponse text,
    in pNavegador varchar(200),
    in pSO varchar(45),
    in pFecCreated datetime
)
BEGIN

	INSERT INTO LOGS(Username,Endpoint,IP,Metodo,Request,Descripcion,Response,Navegador,SO,FecCreated)
    VALUES (pUsername,pEndpoint,pIP,pMetodo,pRequest,pDescripcion,pResponse,pNavegador,pSO,pFecCreated);

END

// 

delimiter ;