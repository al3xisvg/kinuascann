use zinoutvg$HZ;

delimiter //

CREATE PROCEDURE `sp_usuario_obtener`(
	in pUsername varchar(100)
)
BEGIN

	-- Validacion de Errores --
    declare _error int default 0;	
    loop_err: BEGIN 
		set @exists_user := 0;
        
        select count(*) into @exists_user from USUARIOS where Username=pUsername; 
		if @exists_user < 1 then set _error := 301; leave loop_err; end if;
        
    END loop_err;
    
    -- Procedimiento --
	if _error = 0 then
    
		SELECT  u.idUsuario,
				u.Username,
				u.FecCreated,
				u.FecModified,
				u.Rol_id,
				u.Estado_id,
						
				p.Nombre,
				p.Paterno,
				p.Materno,
				p.DNI,
				p.FecNac,
				p.Email,
						
				p.Localidad_id
				
		FROM USUARIOS u
		INNER JOIN PERSONAS p on u.Persona_id=p.idPersona
        WHERE u.Username = pUsername;
    
    else 
		SELECT 
			'True'			AS 'Error',
            Codigo			AS 'Error_Code',
            Tipo			AS 'Error_Type',
            Descripcion		AS 'Error_Description'
		FROM ERRORES
        WHERE Codigo = _error;
    end if;

END

//

delimiter ;