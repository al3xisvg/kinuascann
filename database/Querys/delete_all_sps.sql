use zinoutvg$HZ;

drop procedure if exists sp_historial_insertar;
drop procedure if exists sp_info_ubicacionycampo;
drop procedure if exists sp_logs_insertar;
drop procedure if exists sp_resultado_eliminar;
drop procedure if exists sp_resultado_insertar;
drop procedure if exists sp_resultado_listar;
drop procedure if exists sp_usuario_insertar;
drop procedure if exists sp_usuario_listar;
drop procedure if exists sp_usuario_obtener;
drop procedure if exists sp_usuario_suspender;
drop procedure if exists sp_usuario_update;
drop procedure if exists sp_usuario_validar;
