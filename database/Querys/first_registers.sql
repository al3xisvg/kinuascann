use zinoutvg$HZ;

insert into ROLES(Prioridad, Nombre) values (1,'Administrador');
insert into ROLES(Prioridad, Nombre) values (2,'Analista');
insert into ROLES(Prioridad, Nombre) values (3,'Tecnico');
insert into ROLES(Prioridad, Nombre) values (4,'Ejecutivo');
insert into ROLES(Prioridad, Nombre) values (5,'Empleado');
insert into ROLES(Prioridad, Nombre) values (6,'Civil');
-- select * from ROLES;

insert into ESTADOS(Codigo, Nombre) values ('A','Activo');
insert into ESTADOS(Codigo, Nombre) values ('I','Inactivo');
-- select * from ESTADOS;

insert into PROVINCIAS(Codigo,Nombre,Descripcion) 
values ('LIM','Lima','Ciudad capital del Peru');
-- select * from PROVINCIAS;

insert into DISTRITOS(Codigo, Nombre, Descripcion, Provincia_id) 
values ('MOL','La Molina','Sede del centro de investigacion agronoma',1);
insert into DISTRITOS(Codigo, Nombre, Descripcion, Provincia_id) 
values ('ATE','Ate','Distrito de mayor residencia',1);
-- select * from DISTRITOS;

insert into LOCALIDADES(Nombre,Ubicacion,Descripcion, Distrito_id) 
values('Pablo Bonner','Av. La Molina','Cruce Av. La Molina con Av. Javier Prado',1);
insert into LOCALIDADES(Nombre,Ubicacion,Descripcion, Distrito_id) 
values('Apurimac','Santa Clara','Donde el creador vive',2);
-- select * from LOCALIDADES;

insert into CULTIVOS(Codigo,Ancho,Largo,Perimetro,Institucion,Descripcion,Localidad_id,Estado_id) 
values ('C01',45.7,50.4,124.65,'INIA','Primer cultivo de quinua en INIA - La Molina. Contacto: Jonell Soto Jeri', 1, 1);
-- select * from CULTIVOS;

insert into SURCOS(Codigo, Longitud, Cultivo_id) values ('S01',49.4,1);
insert into SURCOS(Codigo, Longitud, Cultivo_id) values ('S02',50.1,1);
insert into SURCOS(Codigo, Longitud, Cultivo_id) values ('S03',48.1,1);
insert into SURCOS(Codigo, Longitud, Cultivo_id) values ('S04',48.5,1);
insert into SURCOS(Codigo, Longitud, Cultivo_id) values ('S05',50.3,1);
insert into SURCOS(Codigo, Longitud, Cultivo_id) values ('S06',50.4,1);
insert into SURCOS(Codigo, Longitud, Cultivo_id) values ('S07',50.5,1);
insert into SURCOS(Codigo, Longitud, Cultivo_id) values ('S08',48.2,1);
-- select * from SURCOS;

insert into ERRORES(Codigo,Tipo,Descripcion,Paquete,UserCreated,FecCreated)
values (100,1,'El usuario ya existe.','sp_usuario_insertar','avasquez',now());
insert into ERRORES(Codigo,Tipo,Descripcion,Paquete,UserCreated,FecCreated)
values (101,1,'Ya se ha registrado un usuario con este numero de DNI.','sp_usuario_insertar','avasquez',now());
insert into ERRORES(Codigo,Tipo,Descripcion,Paquete,UserCreated,FecCreated)
values (102,1,'Ya se ha registrado un usuario con este e-mail.','sp_usuario_insertar','avasquez',now());
insert into ERRORES(Codigo,Tipo,Descripcion,Paquete,UserCreated,FecCreated)
values (103,1,'El usuario ya se encuentra suspendido.','sp_usuario_suspender','avasquez',now());
insert into ERRORES(Codigo,Tipo,Descripcion,Paquete,UserCreated,FecCreated)
values (300,1,'El estado no existe.','sp_usuario_listar','avasquez',now());
insert into ERRORES(Codigo,Tipo,Descripcion,Paquete,UserCreated,FecCreated)
values (301,1,'El usuario no existe.','sp_usuario_suspender','avasquez',now());
insert into ERRORES(Codigo,Tipo,Descripcion,Paquete,UserCreated,FecCreated)
values (302,1,'El surco no existe.','sp_resultado_insertar','avasquez',now());
insert into ERRORES(Codigo,Tipo,Descripcion,Paquete,UserCreated,FecCreated)
values (303,1,'El cultivo no existe.','sp_resultado_listar','avasquez',now());
insert into ERRORES(Codigo,Tipo,Descripcion,Paquete,UserCreated,FecCreated)
values (304,1,'Credenciales incorrectas.','sp_usuario_validar','avasquez',now());
insert into ERRORES(Codigo,Tipo,Descripcion,Paquete,UserCreated,FecCreated)
values (400,1,'La cantidad de resultados no puede exceder la cantidad de Surcos.','sp_resultado_insertar','avasquez',now());
insert into ERRORES(Codigo,Tipo,Descripcion,Paquete,UserCreated,FecCreated)
values (401,1,'La entrada de ids de resultados debe ser un string separado por comas.','sp_resultado_eliminar','avasquez',now());
insert into ERRORES(Codigo,Tipo,Descripcion,Paquete,UserCreated,FecCreated)
values (402,1,'El usuario se encuentra inactivo.','sp_usuario_validar','avasquez',now());
-- select * from ERRORES;