# Instalación del Proyecto KinuaScann - Tesis:

## Requisitos:

* Python 2.7
* Pip 
* Conda 
* Tkinter
* Pyqt4

Primero instalar conda en el equipo:
>> Desde la pagina oficial

Para instalar el proyecto se requiere levantar un entorno virtual con conda:
>> conda create -n [[ env name ]] python=[[ X.X ]] anaconda
>> conda create -n     envks      python=   2.7    anaconda

Luego, activar el entorno virtual:
>> source activate [[ env name ]] / conda activate [[ env name ]]
>> source activate     envks	  / conda activate      envks

Después, instalar pyqt versión 4 para python2.7:
>> sudo apt-get install python-qt4 / conda install qt=4 pyqt=4 (esto instalara pyqt dentro del entorno virtual)

Para ejecutar el proyecto se debe realizar como superusuario:
>> sudo python app.py

Para abrir el diseñador :
>> designer / ./Designer(/Users/alexisvasquez/anaconda2/bin/Designer.app/Contents/MacOS)

Para actualizar el PATH (en Mac):
>> Verificar rutas en PATH: echo $PATH
>> Abrir editor de bash_profile: vi ~/.bash_profile (~)
>> Añadir la ruta al final del archivo: export PATH=$PATH:/Users/alexisvasquez/anaconda2/bin/Designer.app/Contents/MacOS
>> Guardar cambios y actualizar bash_profile: source $HOME/.bash_profile
>> Verificar cambios en el PATH: echo $PATH
>> Ahora se podra abrir el designer desde cualquier lugar: Designer

Para editar archivos con pyqt4 en editor python:
>> ipython
>> import PyQt4

El directorio donde se instala PyQt y sus comandos se encuentran en la ruta:
>> /Users/alexisvasquez/anaconda2/envs/envks/lib/python2.7/site-packages/PyQt4
>> /Users/alexisvasquez/anaconda2/envs/envks/lib/python2.7/site-packages/PyQt5

Añadir pyuic como comando para evitar colocar toda la ruta:
>> cd /usr/local/bin (si no existe, crear: mkdir /usr/local/bin)
>> sudo vi pyuic
>> ipython /Users/alexisvasquez/anaconda2/envs/envks/lib/python2.7/site-packages/PyQt4/uic/pyuic.py $1
>> Guardar cambios y asignar permisos de ejecucion: sudo chmod +x pyuic
>> Ahora en vez de ejecutar toda esa linea , solo hacer: pyuic fileUI.ui > filePY.py

Para exportar .ui a .py:
>> pyuic guide_v1.ui > ks_v1.py

For open Designer with Python2.7 and Anaconda and Pyqt4:
>> designer

Instalar opencv para python2 en el entorno virtual:
>> conda install -c menpo opencv

Correr automaticamente programa en Raspberry:  
cd /home/pi/.config  --- ver programas agregados (ks.desktop)  

## Database 

Para acceder a la base de datos desde la terminal o linea de comandos de pythonanywhere:
>> mysql -u al3xisvg -h al3xisvg.mysql.pythonanywhere-services.com -p
>> mysql -u [[username]] -h [[database_host_address]] -p  

Para ejecutar query desde la terminal de pythonanywhere:
>> mysql -u al3xisvg -h al3xisvg.mysql.pythonanywhere-services.com -p < EER_KinuaScann.sql

Existen funciones dentro de database/functions que requieren entrar a la terminal mysql de una forma especial:
>> mysql -sN -u al3xisvg -h al3xisvg.mysql.pythonanywhere-services.com -p
(Esto debido a que no se quiere que la salida envie las cabeceras, solo los valores)

## Tablas y Datos

[ ] Los stores sp_historial_insertar y sp_logs_insertar solo tienen ese metodo, ya que el borrado y depuracion
se haran desde la misma base de datos.

## Movil

[ ] Para generar el template en ionic con formato tutorial se debe escribir el siguiente comando:
>> ionic start nombre_proyecto tabs --type=ionic-angular

[ ] El proyectyo de movil esta hecho con npm v6.x , nodejs 10.6.x, ionic 4 y angular 4

[ ] Instalar NVM para manejar diferentes versiones: 
>> curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
>> cd ~/.nvm
>> ./install.sh
>> source ~/.profile
>> nvm --version

[ ] Preparar ambiente para movil
>> nvm install node
>> nvm list 
>> nvm use node || nvm run node --version


RIGHT --> npm install -g ionic@4.1.2 | 
RIGHT --> npm install -g cordova
RIGHT --> cd movil > ionicKS  
RIGHT ..> cordova platform add android
RIGHT ..> cordova platform add ios

COLOR FONDO DE LOGO -> F1E3B1


Para crear logos para el splash screen --> ionic cordova resources
