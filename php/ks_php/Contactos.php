<html>
	<head>
		<meta charset='UTF-8'>
		<link rel="stylesheet" href="css/index.css">
		
		<!-- Uso de Bootstrap CSS -->
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1,minimum-scale=1">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	
		<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
		<script src='js/index.js'></script>	
		
		<script>
			$(".nav li").on("click",function() {
				$(".nav li").removeClass("active");
				$(this).addClass("active");
			})
		</script>
		
		<script>
			function invisible(element) {
				var  elemento = document.getElementById(element);
				elemento.style.display='none';
			}
		</script>
		
	</head>
<body background='img/fondo.jpg'>

	<!-- CABECERA EN LAS PAGINAS -->
	<nav class="navbar navbar-default" role="navigation">
	<!-- El logotipo y el icono que despliega el men� se agrupan para mostrarlos mejor en los dispositivos m�viles -->
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Kinua-SCANN</a>
		</div>
 
		<!-- Agrupar los enlaces de navegaci�n, los formularios y cualquier otro elemento que se pueda ocultar al minimizar la barra -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><a href="Cultivo_root.php">Inicio</a></li>
				<li><a href="Producto.php">Producto</a></li>
				<li class="active"><a href="#">Contactos</a></li>
				<li><a href="Resultados.php">Resultados</a></li>
				<li><a href="Reportes.php">Reportes</a></li>
				<li><a href="Registros.php">Registros</a></li>
			</ul>
 
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.html">Cerrar Sesi&oacute;n</a></li>
			</ul>
		</div>
	</nav>

	<!-- TITULOS -->
	<div class="page-header">
		<h1>
			<p class="text-center text-warning"><b>KINUA-SCANN</b></p>
		</h1>
		<h3>
			<p class="text-center text-muted">upcmonterrico2016.hol.es</p>
		</h3>
	</div>
	
	<!-- CONTENIDO INFORMACION DE CONTACTOS --> 
	<div class="row">
		
		<div class="col-md-offset-1 col-md-3">
			<div class="thumbnail" style="border-radius:30px;">
				<img src="img/henry.png" class="img-thumbnail" style="border-radius:30px;">
				<div class="caption">
					<h3 class="text-center">Alexis V&aacute;squez Garc&iacute;a</h3>
					<p class="text-center">Estudiante de Ingenier&iacute;a Electr&oacute;nica.</p>
					<p class="text-center">D&eacute;cimo Ciclo.</p>
					<p class="text-center">Universidad Peruana de Ciencias Aplicadas.</p>
					<br/>
				</div>	
			</div>
		</div>
  
		<div class="col-md-4">
			<div class="thumbnail" style="border-radius:30px;">
				<img src="img/jonell.png" class="img-thumbnail" style="border-radius:30px;">
				<div class="caption">
					<h3 class="text-center">Jonell Soto Jer&iacute;</h3>
					<p class="text-center">Agr&oacute;nomo Especialista del Programa Nacional de Innovaci&oacute;n Agraria de Cultivos Andinos.</p> 
					<p class="text-center">(Instituci&oacute;n Nacional de Innovaci&oacute;n Agraria &#45; INIA)</p>
					<br/>
				</div>
			</div>
		</div>
  
		<div class="col-md-3">
			<div class="thumbnail" style="border-radius:30px;">
				<img src="img/ore.png" class="img-thumbnail" style="border-radius:30px;">
				<div class="caption">
					<h3 class="text-center">Gian Carlos Or&eacute; Huacles</h3>
					<p class="text-center">Estudiante de Ingenier&iacute;a Electr&oacute;nica.</p>
					<p class="text-center">D&eacute;cimo Ciclo.</p>
					<p class="text-center">Universidad Peruana de Ciencias Aplicadas.</p>
					<br/>
				</div>
			</div>
		</div>
  
	</div>
		
	
</body>
</html>