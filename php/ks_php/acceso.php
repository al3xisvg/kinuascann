<html lang="es">
	<head>
		<meta charset='UTF-8'>
		
		<!-- Uso de Bootstrap CSS -->
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1,minimum-scale=1">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		
		<!-- JavaScript Modal -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
		<script src='js/index.js'></script>
	
	</head>
	
	<body background='img/fondo.jpg'>
	
		<div class="modal fade" id="mostrarmodal" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
				
					<div class="modal-header">
						<h3>ACCESO A KINUA-SCANN</h3>
					</div>
					
					<div class="modal-body">
						<h4>Bienvenido a la ventana emergente de la p&aacute;gina de Kinua-SCANN.</h4>
						Para entrar al contenido, por favor, ingrese su c&oacute;digo v&aacute;lido.
						
						<form class="form-horizontal" role="form" action="acceso.php" method="post">
							<div class="form-group">
								<label class="col-lg-2 control-label">Estado:</label>
								<div class="col-lg-10">
									<p class="form-control-static">Agr&oacute;nomo - INIA - Sede Lima</p>
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword" class="col-lg-2 control-label">C&oacute;digo:</label>
								<div class="col-lg-10">
									<input type="password" class="form-control" name="txtCodigo" placeholder="C&oacute;digo">
								</div>
							</div>
							<input type='submit' class='btn btn-danger btn-large' value='ACCEDER'>
						</form>
						
					</div>
					
					<div class="modal-footer">
						<button type='button' class='btn btn-warning btn-large' onclick="location='index.html'">VOLVER</button>
					</div>
				</div>
			</div>
		</div>
		
		<?php 
			if( isset($_REQUEST['txtCodigo']) ) {
				require_once "Agronomo/AgronomoDAO.php";
				$dao = new AgronomoDAO();
				
				/*TODA LA DATA RECOGIDA DE LA BD EN HOSTINGER*/
				$arreglo = $dao->buscarPorCodigo($_REQUEST['txtCodigo']);
			
				if($arreglo==0) {
					echo " ";
					echo "<h3><b><i>Codigo incorrecto.!!!</i></b></h3>";
					echo "<h5><b><i>Intente Nuevamente.</i></b></h5>";
					echo " ";
				} else {
					echo " ";
					//echo "<h3><b><i>w.!!!</i></b></h3>";
					header("Location:Cultivo_root.php");
					echo " ";
				}
			}
		?>		
		
	</body>
</html>