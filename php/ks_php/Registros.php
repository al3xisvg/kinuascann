<html>
	<head>
		<meta charset='UTF-8'>
		<link rel="stylesheet" href="css/index.css">
		
		<!-- Uso de Bootstrap CSS -->
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1,minimum-scale=1">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap-datetimepicker.css">
	
		<!-- Uso de DatePicker JQuery-->
		<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
		<script src='js/index.js'></script>	
		<script src='js/bootstrap.js'></script>
		<script src="js/bootstrap-datepicker.js"></script>
		
		<script>
			$(".nav li").on("click",function() {
				$(".nav li").removeClass("active");
				$(this).addClass("active");
			})
		</script>
		
		<!-- ABRIR PESTA�A ALTERNATIVA -->
		<script language='JavaScript'>
			function abrirEnPestana(url) {
				window.open(url,'Modificar Datos Cultivo','toolbar=no,scrollbars=no,resizable=no,top=300,left=300,width=320,height=300');
			}
			
			function popup(num) {
				var url="Cultivo/CI_Cultivo_Modificar.php?id="+num;
				abrirEnPestana(url);
			}
		</script>
				
	</head>
<body background='img/fondo.jpg'>

	<!-- CABECERA EN LAS PAGINAS -->
	<nav class="navbar navbar-default" role="navigation">
	<!-- El logotipo y el icono que despliega el men� se agrupan para mostrarlos mejor en los dispositivos m�viles -->
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Kinua-SCANN</a>
		</div>
 
		<!-- Agrupar los enlaces de navegaci�n, los formularios y cualquier otro elemento que se pueda ocultar al minimizar la barra -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><a href="Cultivo_root.php">Inicio</a></li>
				<li><a href="Producto.php">Producto</a></li>
				<li><a href="Contactos.php">Contactos</a></li>
				<li><a href="Resultados.php">Resultados</a></li>
				<li><a href="Reportes.php">Reportes</a></li>
				<li class="active"><a href="#">Registros</a></li>
			</ul>
 
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.html">Cerrar Sesi&oacute;n</a></li>
			</ul>
		</div>
	</nav>

	<!-- TITULOS -->
	<div class="page-header">
		<h1>
			<p class="text-center text-warning"><b>KINUA-SCANN</b></p>
		</h1>
		<h3>
			<p class="text-center text-muted">upcmonterrico2016.hol.es</p>
		</h3>
	</div>
	
	<!-- CONTENIDO -->
	<h3 class="text-center"><center><font color="blue"> INGRESAR DATOS DE PORCENTAJE DE DA&Ntilde;O EN PLANTAS DE QUINUA </font></center></h3>

	<form method="post" action="Registros.php" enctype="multipart/form-data">
		Ingresa el archivo:
		<input name="texto" type="file"/>
		<input name="sape" type="submit" value="Enviar"/>
	</form>
	
	<br/>	
		
	<!--ENLACE CON LA BASE DE DATOS - Data Access Object -->
	<?php 
		if( isset($_REQUEST['sape']) ) {
			
			require_once "Cultivo/CultivoDAO.php";
			require_once "Cultivo/Cultivo.php";
			
			$archivo = fopen($_FILES['texto']['tmp_name'],'r');
			
			if ($archivo==false) {
				echo "<h3><b><i>Error al abrir el archivo.</i></b></h3>";
			}
			else {
				echo "<h3><b><i>SI SE PUDO</i></b></h3>";
			}
			
			$vo = new Cultivo();
			
			$dao = new CultivoDAO();
			
			while(!feof($archivo)) {
				$separador = explode("/",fgets($archivo));
			
				$NumSurco = $separador[0];
				$Vegetacion = $separador[1];
				$Amarilleamiento = $separador[2];
				$Latitud = $separador[3];
				$Longitud = $separador[4];
				$Fecha = $separador[5];
				
				echo "$NumSurco , $Vegetacion , $Amarilleamiento , $Latitud , $Longitud , $Fecha";
				
				echo "<br/>";
				
				$vo->NumSurco = $NumSurco;
				$vo->Vegetacion = $Vegetacion;
				$vo->Amarilleamiento = $Amarilleamiento;
				$vo->Latitud = $Latitud;
				$vo->Longitud  = $Longitud;
				$vo->Fecha = $Fecha;
						
				$dao->insertar($vo);
					
				header("Location: Registros.php?msg=OK");
				
			}		
			fclose($archivo);
			exit();
			echo "<script>alert('Registro Satisfactorio')</script>";
		}
	?>

	
</body>
</html>