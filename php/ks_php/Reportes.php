<html lang="es">

<head>
	<meta charset='UTF-8'>
	<link rel="stylesheet" href="css/index.css">
		
	<!-- Uso de Bootstrap CSS -->
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1,minimum-scale=1">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	
	<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
	<script src='js/index.js'></script>	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
			
	<!-- GRAFICAS -->
	<script src='js/Chart.js'></script>
		
	<script>
		$(".nav li").on("click",function() {
			$(".nav li").removeClass("active");
			$(this).addClass("active");
		})
	</script>
	
	<!-- LLENAR DATOS -->
	<script language='JavaScript'>
		
		/*Variables de Entorno*/
		var vectorIdes=[], vectorParces=[], vectorVeg=[], vectorAmar=[], vectorLat=[], vectorLong=[], vectorFec=[];
		var cantSurcos=5;
		
		/*---------------------*/
			
		function llenar(valor1,valor2,valor3,valor4,valor5,valor6,valor7) {
			vectorIdes.push(valor1);
			vectorParces.push(valor2);
			vectorVeg.push(valor3);
			vectorAmar.push(valor4);
			vectorLat.push(valor5);
			vectorLong.push(valor6);
			
			var sape=new Date(valor7);
			fecha=sape.getFullYear()+'-'+sape.getMonth()+'-'+sape.getDate();
			vectorFec.push(fecha);
			
		}
		
		function graficaPie(valor) {
			var pieData_amar = [];
			var pieData_veg = [];
			var colorcillo = ["#CC3300","#66FF66","#993399","#FFFF66","#6633FF","#FF3300","#9966FF","#FFCCFF","#006666"];
			var cont=1;
			var fin=cantSurcos*valor;
			var ini=fin-cantSurcos+1;
			for(var i=ini-1;i<fin;i++) {
				if (cont==9) {
					cont=1;
				}
				<!-- Vector Amarilleamiento -->
				var val_amar=vectorAmar[i];
				var vec_amar={value:val_amar,color:colorcillo[cont],highlight:"#0c62ab",label:"Amarilleamiento:"};
				pieData_amar.push(vec_amar);
				
				<!-- Vector Vegetacion -->
				var val_veg=vectorVeg[i];
				var vec_veg={value:val_veg,color:colorcillo[cont],highlight:"#0c62ab",label:"Vegetacion:"};
				pieData_veg.push(vec_veg);
				
				cont=cont+1;
			}
			
			var ctx = document.getElementById("chart-area-pie-amar").getContext("2d");
			window.myPie_amar = new Chart(ctx).Doughnut(pieData_amar);			
			
			var ctx1 = document.getElementById("chart-area-pie-veg").getContext("2d");
			window.myPie_veg = new Chart(ctx1).Pie(pieData_veg);
		}
			
		function graficaBar(valor) {
			var etiquetas = [];
			var tmpVeg = [];
			var tmpAmar = [];
			var fin=cantSurcos*valor;
			var ini=fin-cantSurcos+1;
			for (var i=ini-1; i<fin; i++) {
				etiquetas.push('Parcela '+vectorParces[i]);
				tmpVeg.push(vectorVeg[i]);
				tmpAmar.push(vectorAmar[i]);
			}
			var izq = {fillColor : "#6b9dfa",strokeColor : "#ffffff",highlightFill: "#1864f2",highlightStroke: "#ffffff",data: tmpVeg};
			var der = {fillColor : "#e9e225",strokeColor : "#ffffff",highlightFill: "#ee7f49",highlightStroke: "#ffffff",data: tmpAmar};
			var barChartData = {labels : etiquetas, datasets : [izq,der]};
			
			var ctx3 = document.getElementById("chart-area-bar").getContext("2d");
			window.myPie = new Chart(ctx3).Bar(barChartData, {responsive:true});
			
			<!--ctx3.clearRect(0,0,canvas.width,canvas.height);-->
			<!--ctx3.restore();-->
			
			<!--myPie.destroy();-->
			
			<!--myPie.surface.removeAll();-->
			<!--myPie.redraw(false);-->
			<!--myPie.clear();-->
				
		}
			
	</script>
	
	<!-- OBTENER ID DE SELECCION DE FECHAS -->
	<script type="text/javascript">
		$(document).ready(function() {
			$("#my_select").on('change',function(){
				<!--alert($(this).find('option:selected').attr('id'));-->
			});
		});
	</script>
	
</head>

<body background='img/fondo.jpg'> 

<!-- CABECERA EN LAS PAGINAS -->
	<nav class="navbar navbar-default" role="navigation">
	<!-- El logotipo y el icono que despliega el men� se agrupan para mostrarlos mejor en los dispositivos m�viles -->
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Kinua-SCANN</a>
		</div>
 
		<!-- Agrupar los enlaces de navegaci�n, los formularios y cualquier otro elemento que se pueda ocultar al minimizar la barra -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><a href="Cultivo_root.php">Inicio</a></li>
				<li><a href="Producto.php">Producto</a></li>
				<li><a href="Contactos.php">Contactos</a></li>
				<li><a href="Resultados.php">Resultados</a></li>
				<li class="active"><a href="#">Reportes</a></li>
				<li><a href="Registros.php">Registros</a></li>
			</ul>
 
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.html">Cerrar Sesi&oacute;n</a></li>
			</ul>
		</div>
	</nav>

	<!-- TITULOS -->
	<div class="page-header">
		<h1>
			<p class="text-center text-warning"><b>KINUA-SCANN</b></p>
		</h1>
		<h3>
			<p class="text-center text-muted">upcmonterrico2016.hol.es</p>
		</h3>
	</div>
	
	<!--CONTENIDO-->
	<h3><center><font color="blue"> RESULTADOS DE PORCENTAJE DE DA&Ntilde;O EN PLANTAS DE QUINUA </font></center></h3>
	
	<form action="Reportes.php" method="post">
		<div align="right">
			<p><input class='btn btn-primary btn-lg' type='submit' name='vergraficas' value='Visualizar las Graficas'></p>
		</div>
	</form>	
	
	<!--ENLACE CON LA BASE DE DATOS - Data Access Object -->
		<?php 
			if( isset($_POST['vergraficas']) ) {
				
				require_once "Cultivo/CultivoDAO.php";
				$dao = new CultivoDAO();
			
				/*TODA LA DATA RECOGIDA DE LA BD EN HOSTINGER*/
				$arreglo = $dao->listar("");
				if($arreglo==0) {
					echo "<h3><b><i>No hay datos existentes</i></b></h3>";
					echo " ";
				} else {
					foreach ( $arreglo as $n ) {
						$ides=$n->idCultivo;
						$parces=$n->NumSurco;
						$vege=$n->Vegetacion;
						$amar=$n->Amarilleamiento;
						$lat=$n->Latitud;
						$long=$n->Longitud;
				
						$fec=$n->Fecha;
						$fecha=strtotime('+2 day',strtotime($fec)*1000);
						echo "<script>";
						echo "llenar($ides,$parces,$vege,$amar,$lat,$long,$fecha);";
						echo "</script>";
					}		
				}
				
				/*TODA LA DATA RECOGIDA DE LA BD EN HOSTINGER*/
				$arreglo = $dao->listarFechas("");
				
				echo "<select id='my_select' size='4'>";
				
				if($arreglo==0) {
					echo "<h3><b><i>No hay datos existentes</i></b></h3>";
					echo " ";
				} else {
					$cont=1;
					foreach ( $arreglo as $n ) {
						$fec=$n->Fecha;
						echo "<option id='$cont' onclick='JavaScript:graficaPie($cont),graficaBar($cont)'>$fec</option>";
						$cont=$cont+1;
					}		
				}
				echo "</select>";
				
				echo "<br/>";
				echo "<br/>";
			}
		?>
		
		
	<!-- GRAFICA PIE -->		
	<div class="row">
		<div class="col-md-offset-1 col-md-10">
			<canvas id="chart-area-pie-veg" width="300" height="300"></canvas>
			<canvas id="chart-area-pie-amar" width="300" height="300"></canvas>
		</div>
	</div>
		
	<!--GRAFICA BARRAS-->
	<div class="row">
		<div class="col-md-offset-1 col-md-10">		
			<!--<div id="canvas-holder">-->
			<canvas id="chart-area-bar" width="600" height="300"></canvas>
			<!--</div>-->
		</div>
	</div>
	
</body>
</html>
