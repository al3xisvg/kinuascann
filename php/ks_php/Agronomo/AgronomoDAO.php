<?php 
require_once "Agronomo.php";
require_once "funciones.php";

class AgronomoDAO {
	
	public function buscarPorCodigo($sape){	
		try { 	
			$db = conectar();
			$stmt = $db->prepare("SELECT idAgronomo, Codigo, Nombre, Paterno, Materno, Edad, DNI, idCultivo 
                                  FROM Agronomo 
                                  WHERE Codigo LIKE ?");
			$stmt->bindValue(1, "%$sape%", PDO::PARAM_STR);
			$stmt->execute();
			
			$filas = $stmt->fetchAll(PDO::FETCH_ASSOC);			
			foreach($filas as $fila) {			
				$vo = new Agronomo();
				$vo->idAgronomo = $fila['idAgronomo'];
				$vo->Codigo = $fila['Codigo'];
				$vo->Nombre = $fila['Nombre'];
				$vo->Paterno = $fila['Paterno'];
				$vo->Materno = $fila['Materno'];
				$vo->Edad = $fila['Edad'];
				$vo->DNI = $fila['DNI'];
				$vo->idCultivo = $fila['idCultivo'];
				$objetos[] = $vo;
			}
			return $objetos;
			
		} catch (PDOException $e) {
			$db->rollback();
			$mensaje  = '<b>Consulta invÃ¡lida:</b> ' . $e->getMessage() . "<br/>";
			$mensaje .= '<b>Consulta: </b>' . $sql;
			die($mensaje);
		}		
    }
	
}

?>