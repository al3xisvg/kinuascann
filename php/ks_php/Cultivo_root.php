<html>
	<head>
		<meta charset='UTF-8'>
		<link rel="stylesheet" href="css/index.css">
		
		<!-- Uso de Bootstrap CSS -->
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1,minimum-scale=1">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	
		<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
		<script src='js/index.js'></script>	
		
		<script>
			$(".nav li").on("click",function() {
				$(".nav li").removeClass("active");
				$(this).addClass("active");
			})
		</script>
		
	</head>
<body background='img/fondo.jpg'>

	<!-- CABECERA EN LAS PAGINAS -->
	<nav class="navbar navbar-default" role="navigation">
	<!-- El logotipo y el icono que despliega el men� se agrupan para mostrarlos mejor en los dispositivos m�viles -->
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Kinua-SCANN</a>
		</div>
 
		<!-- Agrupar los enlaces de navegaci�n, los formularios y cualquier otro elemento que se pueda ocultar al minimizar la barra -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">Inicio</a></li>
				<li><a href="Producto.php">Producto</a></li>
				<li><a href="Contactos.php">Contactos</a></li>
				<li><a href="Resultados.php">Resultados</a></li>
				<li><a href="Reportes.php">Reportes</a></li>
				<li><a href="Registros.php">Registros</a></li>
			</ul>
 
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.html">Cerrar Sesi&oacute;n</a></li>
			</ul>
		</div>
	</nav>

	<!-- TITULOS -->
	<div class="page-header">
		<h1>
			<p class="text-center text-warning"><b>KINUA-SCANN</b></p>
		</h1>
		<h3>
			<p class="text-center text-muted">upcmonterrico2016.hol.es</p>
		</h3>
	</div>
	
	<!-- INICIO -->
	<div class="row">
		<!--MAXIMO NUMERO DE COLUMNAS PANTALLA = 12 -->
		<!--col-md-xx es para dipositivos medianos. Tablets y ordenadores-->
		<!--Se desplaza 2 columnas en la pantalla a la izquierda.-->
		<!--Quedan 10 columnas a la derecha-->
		<!--Para que quede en el centro se quitan a esas 10 restantes el offset-->
		<!--Formula: offset+md+Resto=12 (Resto=offset - paddings)-->
		<!--Ejm: 3+md+3=12 entonces md=6 -->
		<div class="col-md-offset-3 col-md-6">
			<div class="panel panel-success" style="border-radius:50px;">
				<div class="panel-heading text-center" style="border-radius:30px;">
					<h3 class="panel-title"><b>Bienvenido a la P&aacute;gina de Inicio de Kinua-SCANN</b></h3>
				</div>
				<div class="panel-body" style="background:#151414; border-radius:30px;">
					<p class="text-justify" style="color:white">
						Bienvenido a la P&aacute;gina de Inicio para la Validaci&oacute;n de Porcentaje de da&ntilde;o, 
						provocado por la enfermedad del Mildiu presente en las plantas de quinua
						en la costa del Instituto Nacional de Innovaci&oacute;n Agraria (INIA - Sede Lima).
					</p>
					<br/>
					<p class="text-center"><img src="img/presentacion.png" alt="Equipo Prototipo Kinua-SCANN" width=225 height=180></p>
				</div>
			</div>		
		</div>
	</div>
	
</body>
</html>