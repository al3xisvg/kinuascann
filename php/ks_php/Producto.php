<html>
	<head>
		<meta charset='UTF-8'>
		<link rel="stylesheet" href="css/index.css">
		
		<!-- Uso de Bootstrap CSS -->
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1,minimum-scale=1">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	
		<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
		<script src='js/index.js'></script>	
		
		<script>
			$(".nav li").on("click",function() {
				$(".nav li").removeClass("active");
				$(this).addClass("active");
			})
		</script>
		
	</head>
<body background='img/fondo.jpg'>

	<!-- CABECERA EN LAS PAGINAS -->
	<nav class="navbar navbar-default" role="navigation">
	<!-- El logotipo y el icono que despliega el menú se agrupan para mostrarlos mejor en los dispositivos móviles -->
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Kinua-SCANN</a>
		</div>
 
		<!-- Agrupar los enlaces de navegación, los formularios y cualquier otro elemento que se pueda ocultar al minimizar la barra -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><a href="Cultivo_root.php">Inicio</a></li>
				<li class="active"><a href="#">Producto</a></li>
				<li><a href="Contactos.php">Contactos</a></li>
				<li><a href="Resultados.php">Resultados</a></li>
				<li><a href="Reportes.php">Reportes</a></li>
				<li><a href="Registros.php">Registros</a></li>
			</ul>
 
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.html">Cerrar Sesi&oacute;n</a></li>
			</ul>
		</div>
	</nav>

	<!-- TITULOS -->
	<div class="page-header">
		<h1>
			<p class="text-center text-warning"><b>KINUA-SCANN</b></p>
		</h1>
		<h3>
			<p class="text-center text-muted">upcmonterrico2016.hol.es</p>
		</h3>
	</div>
	
	<!-- ETIQUETAS PARA EL PRODUCTO -->
	<h3 align="center">
		<span class="label label-primary text-center">¿En qué consiste el proyecto?</span>
		<span class="label label-primary text-center">¿Qué soluciona?</span>
		<span class="label label-primary text-center">¿Cómo se implementó el Proyecto?</span>
	</h3>
	<hr style="color:#0056b2;"/>
	
	<!-- CONTENIDO INFORMACION DEL PRODUCTO -->
	<div class="row">
		<div class="col-md-offset-1 col-md-10">					
			<ul class="media-list">	
				<!-- PRIMER EVENTO -->
				<li class="media">
					<div class="media-left">
						<img class="media-object" src="img/pantalla.png" alt="Imagen 1">
					</div>
					<div class="media-body">
						<h4 class="media-heading">El Proyecto Kinua Scann consiste en...</h4>
						<p class="text-justify">
							...el desarrollo de un sistema portátil electrónico de adquisición de imágenes
							digitales orientado a extraer información de interés para el diagnóstico de 
							la enfermedad del Mildiu presente en las hojas de quinua de la costa para el
							Instituto Nacional de Innovación Agraria - INIA (Sede Lima).
						</p>
					</div>
				</li>
						
				<!-- SEGUNDO EVENTO -->
				<li class="media">	
					<div class="media-left">
						<img class="media-object" src="img/pantalla.png" alt="Imagen 1">
					</div>
					<div class="media-body">
						<h4 class="media-heading">La Situación Problemática surgió a partir de...</h4>
						<p class="text-justify">
							la situación..."El mal manejo de la quinua ha hecho que haya más plagas y el agricultor de la
							costa está acostumbrado a controlar con fungicidas y pesticidas, sin conocer los
							productos adecuados que deberían aplicar. Se ha contaminado la quinua y tenemos un
							stock que ha sido aplicado con productos químicos". (Luz Gómez - UNALM).
						</p>
						<p class="text-justify"><b>Enfermedad:</b> Mildiu. Predominante en las plantas de quinua sembradas en la costa. Se expande de manera 
							exponencial por el aire.
						</p>
						<p class="text-justify"><b>Soluciones actuales:</b> Control cualitativo de la enfermedad y aplicación de herbicidas
							o pesticidas según el criterio del agrónomo.
						</p>
						<p><b>Riesgos:</b></p>
						<p class="text-justify">
							- De aplicar <b>mucho herbicida</b> provocaría efecto negativo en las plantas como su 
							<b>deshojamiento.</b>
						</p>
						<p class="text-justify">- De aplicar <b>poco herbicida</b> provocaría que la enfermedad se vuelva más fuerte y se <b>siga
							expandiendo</b> por el cultivo.
						</p>
						<p class="text-justify"> El proyecto busca entregar un método cuantitativo respecto a la obtención de resultados
							del porcentaje de daño de la enfermedad del Mildiu en las plantas de quinua. De esta forma el agrónomo
							podrá tener una data más precisa para determinar cuánto herbicida debe aplicar.
						</p>
					</div>
						
					<div class="media-right">
						<img src="img/luzgomez.png" alt="Imagen 1">
						<center>
							<p><span><b>Fuente:</b> Diario Correo Perú (diariocorreo.pe)</span></p>
							<p><span><b>Figura 1.</b> Dra. Luz Gómez Pando - Jefa del Programa de Cereales de la UNALM.</span></p>
						</center>
					</div>
												
				</li>
			
				<!-- TERCER EVENTO -->
				<li class="media">
					<div class="media-left">
						<img class="media-object" src="img/pantalla.png" alt="Imagen 1">
					</div>
					<div class="media-body">
						<h4 class="media-heading">La implementación se realizó con ayuda de...</h4>
						componentes electrónicos dedicados al procesamiento de información. Tal es el caso del sistema 
						embebido Raspberry Pi3. Este
					</div>	
				</li>
						
				<center><img src="img/componentes.png" alt="Imagen 2"></center>
											
			</ul>
		</div>
				
	</div>	
	
</body>
</html>