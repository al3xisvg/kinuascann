<html>
	<head>
		<meta charset='UTF-8'>
		<link rel="stylesheet" href="css/index.css">
		
		<!-- Uso de Bootstrap CSS -->
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1,minimum-scale=1">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap-datetimepicker.css">
	
		<!-- Uso de DatePicker JQuery-->
		<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
		<script src='js/index.js'></script>	
		<script src='js/bootstrap.js'></script>
		<script src="js/bootstrap-datepicker.js"></script>
		
		<script>
			$(".nav li").on("click",function() {
				$(".nav li").removeClass("active");
				$(this).addClass("active");
			})
		</script>
		
		<!-- ABRIR PESTA�A ALTERNATIVA -->
		<script language='JavaScript'>
			function abrirEnPestana(url) {
				window.open(url,'Modificar Datos Cultivo','toolbar=no,scrollbars=no,resizable=no,top=300,left=300,width=320,height=300');
			}
			
			function popup(num) {
				var url="Cultivo/CI_Cultivo_Modificar.php?id="+num;
				abrirEnPestana(url);
			}
		</script>
				
	</head>
<body background='img/fondo.jpg'>

	<!-- CABECERA EN LAS PAGINAS -->
	<nav class="navbar navbar-default" role="navigation">
	<!-- El logotipo y el icono que despliega el men� se agrupan para mostrarlos mejor en los dispositivos m�viles -->
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Kinua-SCANN</a>
		</div>
 
		<!-- Agrupar los enlaces de navegaci�n, los formularios y cualquier otro elemento que se pueda ocultar al minimizar la barra -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><a href="Cultivo_root.php">Inicio</a></li>
				<li><a href="Producto.php">Producto</a></li>
				<li><a href="Contactos.php">Contactos</a></li>
				<li class="active"><a href="#">Resultados</a></li>
				<li><a href="Reportes.php">Reportes</a></li>
				<li><a href="Registros.php">Registros</a></li>
			</ul>
 
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.html">Cerrar Sesi&oacute;n</a></li>
			</ul>
		</div>
	</nav>

	<!-- TITULOS -->
	<div class="page-header">
		<h1>
			<p class="text-center text-warning"><b>KINUA-SCANN</b></p>
		</h1>
		<h3>
			<p class="text-center text-muted">upcmonterrico2016.hol.es</p>
		</h3>
	</div>
	
	<!-- CONTENIDO -->
	<h3 class="text-center"><center><font color="blue"> RESULTADOS DE PORCENTAJE DE DA&Ntilde;O EN PLANTAS DE QUINUA </font></center></h3>
	
	<div class="row">
		<div class="col-md-offset-4 col-md-4">
			<form role="form">
				<div class="form-group">
					<label>Surco N&deg;:</label>
					<!--<input type="email" class="form-control" id="ejemplo_email_1" placeholder="Introduce tu email">-->
					<select class="form-control" name="txtSurco">
						<option value="1">Surco 1</option>
						<option value="2">Surco 2</option>
						<option value="3">Surco 3</option>
						<option value="4">Surco 4</option>
						<option value="5">Surco 5</option>
					</select>
				</div>
				<div class="form-group">
					<label>Fecha:</label>
					<input  type="date" name="txtFecha">	
				</div>
				
				<div class="row">
					<div class="col-md-offset-0 col-md-4">
						<button type="submit" class="btn btn-primary">Buscar</button>
					</div>
					
					<div class="col-md-offset-0 col-md-4">
						<input class='btn btn-warning' type='submit' name='listar' value='Listar'>
					</div>
				</div>
			</form>
		</div>
	
	</div>
	<table class="table table-hover">
		<center><tr>
			<th>#</td>
			<th bgcolor="orange">idParcela</th>
			<th bgcolor="orange">NumSurco</th>
			<th bgcolor="orange">Vegetacion</th>
			<th bgcolor="orange">Amarilleamiento</th>
			<th bgcolor="orange">Latitud</th>
			<th bgcolor="orange">Longitud</th>
			<th bgcolor="orange">Fecha</th>
			<th bgcolor="orange" colspan="2"><i>Opciones</i></th>
		</tr></center>

	<br/>	
		
	<!--ENLACE CON LA BASE DE DATOS - Data Access Object -->
	<?php 
		if( isset($_REQUEST['txtSurco']) ) {
			require_once "Cultivo/CultivoDAO.php";
			$dao = new CultivoDAO();
			
			/*TODA LA DATA RECOGIDA DE LA BD EN HOSTINGER*/
			$arreglo = $dao->buscarPorSurco($_REQUEST['txtSurco'],$_REQUEST['txtFecha']);
			
			if( isset($_REQUEST['listar']) ) {
				$arreglo = $dao->listar('');
			}
		
			if($arreglo==0) {
				echo "<h3><b><i>No hay datos existentes</i></b></h3>";
				echo " ";
			} else {
				$cont=1;
		
				foreach ( $arreglo as $n ) {
					print "<tr>";
					print "<td> $cont </td>"; 
					print "<td>".$n->idCultivo . "</td>";
					print "<td>".$n->NumSurco . "</td>";
					print "<td>".$n->Vegetacion . "</td>";
					print "<td>".$n->Amarilleamiento . "</td>";
					print "<td>".$n->Latitud . "</td>";
					print "<td>".$n->Longitud . "</td>";
					print "<td>".$n->Fecha . "</td>";
					
					$ID=$n->idCultivo;
					echo "<th><i><a href='javascript:popup($ID)'>Editar</a></i></th>";
					echo "<th><i><a href='Cultivo/CI_Cultivo_Eliminar.php?id=$n->idCultivo'>Eliminar</a></i></th>";
					$cont ++;
				}	
			}
		}			
	?>

	
</body>
</html>