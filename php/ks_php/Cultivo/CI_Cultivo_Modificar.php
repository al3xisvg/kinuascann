<?php
	
require_once "Cultivo.php";
require_once "CultivoDAO.php";

$dao = new CultivoDAO();
$vo = $dao->obtenerInfo($_REQUEST['id']);
?>

<html>

<body>
	<h3><center><font color="white"> Editar Datos de la Parcela </font></center></h3>

	<form action="CC_Cultivo_Actualizar.php" method="post">
	    
		<input type="hidden" name="id" 
			   value= "<?php echo $vo->idCultivo;?>"
		/>
		
		NumSurco: <input type="text" name="txtNumSurco" value="<?php echo $vo->NumSurco; ?>" required/>
		<br/>
		Vegetacion: <input type="text" name="txtVegetacion" value="<?php echo $vo->Vegetacion; ?>" required/>
		<br/>
		Amarilleamiento: <input type="text" name="txtAmarilleamiento" value="<?php echo $vo->Amarilleamiento; ?>" required/>
		<br/>
		Latitud: <input type="text" name="txtLatitud" value="<?php echo $vo->Latitud; ?>" required/>
		<br/>
		Longitud: <input type="text" name="txtLongitud" value="<?php echo $vo->Longitud; ?>" required/>
		<br/>
		Fecha: <input type="text" name="txtFecha" value="<?php echo $vo->Fecha; ?>" required/>
		<br/>
		<br/>
		<br/>			
		<input type="submit" value="Grabar"/>
		<br/>
		<br/>
		<br/>
		<!--<center><input type="button" value="Regresar" onclick="window.location='Mapeo.php'"/></center>-->
	</form>
	
<?php
if ( isset($_REQUEST["msg"]) && $_REQUEST["msg"] == "OK" ) {
  echo "Se actualiz&oacute; exitosamente";  
}
?>	

</body>

</html>