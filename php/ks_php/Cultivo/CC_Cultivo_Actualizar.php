<?php	
	require_once "Cultivo.php";
	require_once "CultivoDAO.php";

	$vo = new Cultivo();
	$vo->idCultivo = $_REQUEST['id'];
	$vo->NumSurco = $_REQUEST['txtNumSurco'];
	$vo->Vegetacion = $_REQUEST['txtVegetacion'];
	$vo->Amarilleamiento = $_REQUEST['txtAmarilleamiento'];
	$vo->Latitud = $_REQUEST['txtLatitud'];
	$vo->Longitud = $_REQUEST['txtLongitud'];
	$vo->Fecha = $_REQUEST['txtFecha'];

	$dao = new CultivoDAO();
	$dao->actualizar($vo);

	header("Location: CI_Cultivo_Modificar.php?msg=OK&id=".$_REQUEST['id']);
	exit();		
?>