<?php 
require_once "Cultivo.php";
require_once "funciones.php";

class CultivoDAO {
	
	public function buscarPorSurco($sape,$sabe){	
		try { 	
			$db = conectar();
			$stmt = $db->prepare("SELECT idCultivo, NumSurco, Vegetacion, Amarilleamiento, Latitud, Longitud, Fecha 
                                  FROM Cultivo
                                  WHERE NumSurco LIKE ? AND
								  Fecha LIKE ?");
			$stmt->bindValue(1, "%$sape%", PDO::PARAM_STR);
			$stmt->bindValue(2, "%$sabe%", PDO::PARAM_STR);
			$stmt->execute();
			
			$filas = $stmt->fetchAll(PDO::FETCH_ASSOC);			
			foreach($filas as $fila) {			
				$vo = new Cultivo();
				$vo->idCultivo = $fila['idCultivo'];
				$vo->NumSurco= $fila['NumSurco'];
				$vo->Vegetacion = $fila['Vegetacion'];
				$vo->Amarilleamiento = $fila['Amarilleamiento'];
				$vo->Latitud = $fila['Latitud'];
				$vo->Longitud = $fila['Longitud'];
				$vo->Fecha = $fila['Fecha'];
				$objetos[] = $vo;
			}
			return $objetos;
			
		} catch (PDOException $e) {
			$db->rollback();
			$mensaje  = '<b>Consulta inv�lida:</b> ' . $e->getMessage() . "<br/>";
			$mensaje .= '<b>Consulta: </b>' . $sql;
			die($mensaje);
		}		
    }
	
	public function buscarPorFechas($sape){	
		try { 	
			$db = conectar();
			$stmt = $db->prepare("SELECT idCultivo, NumSurco, Vegetacion, Amarilleamiento, Latitud, Longitud, Fecha 
                                  FROM Cultivo
                                  WHERE Fecha LIKE ?");
			$stmt->bindValue(1, "%$sape%", PDO::PARAM_STR);
			$stmt->execute();
			
			$filas = $stmt->fetchAll(PDO::FETCH_ASSOC);			
			foreach($filas as $fila) {			
				$vo = new Cultivo();
				$vo->idCultivo = $fila['idCultivo'];
				$vo->NumSurco= $fila['NumSurco'];
				$vo->Vegetacion = $fila['Vegetacion'];
				$vo->Amarilleamiento = $fila['Amarilleamiento'];
				$vo->Latitud = $fila['Latitud'];
				$vo->Longitud = $fila['Longitud'];
				$vo->Fecha = $fila['Fecha'];
				$objetos[] = $vo;
			}
			return $objetos;
			
		} catch (PDOException $e) {
			$db->rollback();
			$mensaje  = '<b>Consulta inv�lida:</b> ' . $e->getMessage() . "<br/>";
			$mensaje .= '<b>Consulta: </b>' . $sql;
			die($mensaje);
		}		
    }
	
	public function obtenerInfo($id){
		try { 	
			$db = conectar();
			$stmt = $db->prepare("SELECT idCultivo, NumSurco, Vegetacion, Amarilleamiento, Latitud, Longitud, Fecha 
			                      FROM Cultivo
								  WHERE idCultivo = ?");
			$stmt->bindValue(1, $id);
			$stmt->execute();
			$fila = $stmt->fetchAll(PDO::FETCH_ASSOC);				
			if($fila) {			
				$vo = new Cultivo();
				$vo->idCultivo = $fila[0]['idCultivo'];
				$vo->NumSurco = $fila[0]['NumSurco'];
				$vo->Vegetacion = $fila[0]['Vegetacion'];
				$vo->Amarilleamiento = $fila[0]['Amarilleamiento'];
				$vo->Latitud = $fila[0]['Latitud'];
				$vo->Longitud = $fila[0]['Longitud'];
				$vo->Fecha = $fila[0]['Fecha'];
			}
			return $vo;
			
		} catch (PDOException $e) {
			$link->rollback();
			$mensaje  = '<b>Consulta inv&#11113;da:</b> ' . $e->getMessage() . "<br/>";
			$mensaje .= '<b>Consulta: </b>' . $sql;
			die($mensaje);
		}	
	}
	
	public function listar($sape){	
		try { 	
			$db = conectar();
			$stmt = $db->prepare("SELECT idCultivo, NumSurco, Vegetacion, Amarilleamiento, Latitud, Longitud, Fecha 
                                  FROM Cultivo
                                  WHERE idCultivo LIKE ?");
			$stmt->bindValue(1, "%$sape%", PDO::PARAM_STR);
			$stmt->execute();
			
			$filas = $stmt->fetchAll(PDO::FETCH_ASSOC);			
			foreach($filas as $fila) {			
				$vo = new Cultivo();
				$vo->idCultivo = $fila['idCultivo'];
				$vo->NumSurco= $fila['NumSurco'];
				$vo->Vegetacion = $fila['Vegetacion'];
				$vo->Amarilleamiento = $fila['Amarilleamiento'];
				$vo->Latitud = $fila['Latitud'];
				$vo->Longitud = $fila['Longitud'];
				$vo->Fecha = $fila['Fecha'];
				$objetos[] = $vo;
			}
			return $objetos;
			
		} catch (PDOException $e) {
			$db->rollback();
			$mensaje  = '<b>Consulta inv�lida:</b> ' . $e->getMessage() . "<br/>";
			$mensaje .= '<b>Consulta: </b>' . $sql;
			die($mensaje);
		}		
    }
	
	public function listarFechas($sape){	
		try { 	
			$db = conectar();
			$stmt = $db->prepare("SELECT DISTINCT Fecha 
                                  FROM Cultivo
                                  WHERE idCultivo LIKE ?");
			$stmt->bindValue(1, "%$sape%", PDO::PARAM_STR);
			$stmt->execute();
			
			$filas = $stmt->fetchAll(PDO::FETCH_ASSOC);			
			foreach($filas as $fila) {			
				$vo = new Cultivo();
				$vo->Fecha = $fila['Fecha'];
				$objetos[] = $vo;
			}
			return $objetos;
			
		} catch (PDOException $e) {
			$db->rollback();
			$mensaje  = '<b>Consulta inv�lida:</b> ' . $e->getMessage() . "<br/>";
			$mensaje .= '<b>Consulta: </b>' . $sql;
			die($mensaje);
		}		
    }
	
	public function actualizar($vo){
		try { 
			$db = conectar(); 			
			$stmt = $db->prepare("UPDATE Cultivo 
								  SET NumSurco=?, Vegetacion=?, Amarilleamiento=?, Latitud=?, Longitud=?, Fecha=?
								  WHERE idCultivo=?");
			$datos = array($vo->NumSurco, $vo->Vegetacion, $vo->Amarilleamiento, $vo->Latitud, $vo->Longitud, $vo->Fecha, $vo->idCultivo);
			$db->beginTransaction();						
			$stmt->execute($datos);			
			$db->commit();
		} catch (PDOException $e) {
			$db->rollback();
			$mensaje  = '<b>Consulta inv&#11113;da:</b> ' . $e->getMessage() . "<br/>";
			$mensaje .= '<b>Consulta: </b>' . $sql;
			die($mensaje);
		}	
	}
	
	public function eliminarInfo($id){
		try { 
			$db = conectar();	  
			$stmt = $db->prepare("DELETE FROM Cultivo WHERE idCultivo=?");
			$datos = array($id);
			$db->beginTransaction();			
			$stmt->execute($datos);			
			$db->commit();
		} catch (PDOException $e) {
			$db->rollback();
			$mensaje  = '<b>Consulta inv&#11113;da:</b> ' . $e->getMessage() . "<br/>";
			$mensaje .= '<b>Consulta: </b>' . $sql;
			die($mensaje);
		}	
	}	
	
	public function insertar($vo){
		try { 
			$db = conectar(); 			
			$stmt = $db->prepare("INSERT INTO Cultivo (NumSurco, Vegetacion, Amarilleamiento, Latitud, Longitud, Fecha) VALUES (?, ?, ?, ?, ?, ?)");
			$datos = array($vo->NumSurco, $vo->Vegetacion, $vo->Amarilleamiento, $vo->Latitud, $vo->Longitud, $vo->Fecha);
			$db->beginTransaction();						
			$stmt->execute($datos);			
			$db->commit();
		} catch (PDOException $e) {
			$db->rollback();
			$mensaje  = '<b>Consulta invalida:</b> ' . $e->getMessage() . "<br/>";
			$mensaje .= '<b>Consulta: </b>' . $sql;
			die($mensaje);
		}		
	}
	
	
}

?>