import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  // -- Constantes Globales --
  private urlLogin: string = 'http://al3xisvg.pythonanywhere.com/auth/login';
  private urlShowUsuario: string = 'http://al3xisvg.pythonanywhere.com/private/user/select';

  // -- Variables Globales --
  private obs_login: Observable<any>;
  private flag_ver: boolean = false;
  private __token: string = '';
  private ngUsername: string = '';
  private ngContrasena: string = '';
  private ngUsuario;

  // -- Constructor --  
  constructor(public navCtrl: NavController, public httpClient: HttpClient, public alertCtrl: AlertController) {

  }

  /* Funcion: viewPassword
  * Argumentos: -
  * Salida: boolean
  * Descripcion: cambia el estado del flag_ver para ver contraseña
  */
  viewPassword() {
	  this.flag_ver = !this.flag_ver;
  }  

  /* Function: login
  * Argumentos: -
  * Salida: object http
  * Descripcion: login apuntando a la api pythoanywhere 
  */
  login() {
  	var headers = new HttpHeaders({
  		'Accept' : 'application/json',
  		'Content-Type' : 'application/json'
  	});
  	const requestOptions = {headers:headers};

  	let postData = {
  		'Username': this.ngUsername,
  		'Contrasena': this.ngContrasena
  	}

  	this.obs_login = this.httpClient.post(this.urlLogin, postData, requestOptions);
  	this.obs_login.subscribe(response => {
  		if (response['code'] == 201) {
  			this.__token = response['data']['access_token'];
  			this.showUsuario();
  		} else {
  			this.__token = '';
  			this.ngUsername = '';
  			this.ngContrasena = '';
  			this.orderHttpResponse(response);
  		}
  	}, error => {
  		console.log(error);
  	});
  }

  /* Function: showUsuario
  * Argumentos: -
  * Salida: object http
  * Descripcion: listar datos de usuario tras el login 
  */
  showUsuario() {
  	var headers = new HttpHeaders({
  		'Accept' : 'application/json',
  		'Content-Type' : 'application/json',
  		'DataToken' : 'Bearer ' + this.__token
  	});
  	const requestOptions = {headers:headers};

  	let postData = {
  		'Username': this.ngUsername
  	}

  	this.obs_login = this.httpClient.post(this.urlShowUsuario, postData, requestOptions);
  	this.obs_login.subscribe(response => {
  		if (response['code'] == 201) {
  			this.ngUsername = response['data']['Username'];
  			this.ngContrasena = '';
  			this.ngUsuario = response['data'];
  			
  			this.Notify('Éxito!','custom-success','Bienvenido estimado ' + response['data']['Nombre'] + '.');
  		} else {
  			this.__token = '';
  			this.ngUsername = '';
  			this.ngContrasena = '';
  			this.orderHttpResponse(response);
  		}
  	}, error => {
  		console.log(error);
  	});
  }

  /* Function: Notify
  * Argumentos: msg
  * Salida: modal
  * Descripcion: Muestra un anuncio modal
  */
  async Notify(titulo,custom_tipo,msg) {
    const alert = await this.alertCtrl.create({
      title: titulo,
      cssClass: custom_tipo,
      message: msg,
      buttons: [{
      	text: 'Ok',
      	cssClass: 'custom-button'
      }]
    });
    await alert.present();
  }

  /* Function: orderHttpResponse
  * Argumentos: http_response
  * Salida: modal
  * Descripcion: muestra los errores del request en una modal
  */
  orderHttpResponse(http_response) {
  	var err_war = ('error' in http_response)?'error': (('warning' in http_response)?'warning':'');
  	switch(err_war) {
        case 'error': {
            this.Notify('Error!','custom-error',http_response['error']);
            break;
        }
        case 'warning': {
            if (typeof http_response['warning']=='object') {
                this.Notify('Advertencia!','custom-warning',http_response['warning']['Error_Description']);
            } else {
                this.Notify('Advertencia!','custom-warning',http_response['warning']);
            }
            break;
        }
        default: {
            this.Notify('Alerta!','custom-warning',http_response['data']);
            break;
        }  
    }
  }

}
