var app = angular.module('home',[]);

app.controller('homeController',function($scope,$http,$window) {

	// ---- Declaracion de Constantes ----
	loginUri = uriBase + '/auth/login';
	getDataUserUri = uriBase + '/private/user/sel';
	token = '';

	// ---- Declaracion de Variables ----
	$scope.ngUsuario = {};

	// ---- Funciones ----

	/* Function: Login
     * Arguments: 
     * Output: -
     * Method: -
     * Summary: logear con usuario y contraseña
     */
	$scope.login = function() {
		$http({
            url:loginUri,
            method:'POST',
            headers: {
                'Content-Type':'application/json'
            },
            data: {
                'Username': $scope.ngUsuario.Username,
                'Contrasena': $scope.ngUsuario.Password
            },
        }).then(function(exito) {
        	if(exito.data['code'] != 201) {
        		$scope.ngUsuario.Username = '';
        		$scope.ngUsuario.Password = '';
       			Notify(exito.data['warning']['Error_Description'], 'warning'); 		
        	} else {
        		$scope.ngUsuario.Password = '';
				token = exito.data['data']['access_token'];
				//$scope.getDataUser(token);
				$window.localStorage.setItem("current_token", token);
        	}
        },function(error) {
        	$scope.ngUsuario.Username = '';
        	$scope.ngUsuario.Password = '';
            Notify('Ocurrio un error en el Login ! ', 'error');
        })
	}

	/* Function: Login
     * Arguments: 
     * Output: current_token, current_user (variables globales)
     * Method: -
     * Summary: logear con usuario y contraseña
     */
	$scope.getDataUser = function(pToken) {
		$http({
            url:getDataUserUri,
            method:'POST',
            headers: {
                'Content-Type':'application/json',
                'DataToken': 'Bearer '+pToken
            },
            data: {
                'Username': $scope.ngUsuario.Username
            },
        }).then(function(exito) {
        	if(exito.data['code'] != 201) {
       			Notify('No se encontraron datos de usuario ! ', 'warning'); 		
        	} else {
        		var lista = ['idUsuario','Nombre','Paterno','Materno','FecNac','DNI','EmailDir']
        		for(var i=0; i<lista.length; i++){
        			$scope.ngUsuario[lista[i]] = exito.data['data'][i];
        		}
        		$window.localStorage.setItem("current_user", JSON.stringify($scope.ngUsuario));
        		$window.location.href = 'listarsolicitud.html'
        	}
        },function(error) {
            Notify('Ocurrio un error al obtener datos de usuario ! ', 'error');
        })	
	}

	/* Function: Notify (Error/Success)
     * Arguments: 
     * Output: -
     * Method: -
     * Summary: show a message with error in proccess
     */
     function Notify(msg,tipo){
        new PNotify({ 
                        text: msg,
                        hide: true,
                        type: tipo,
                        styling: 'bootstrap3',
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });

     }

});