var app = angular.module('index',[]);

app.controller('indexController',function($scope,$http) {

	// Endpoints - Api:
	var urlLogin = 'http://al3xisvg.pythonanywhere.com/auth/login';
	var urlInfoUbicYCampo = 'http://al3xisvg.pythonanywhere.com/public/info/ubicycamp';
     var urlShowUsuario = 'http://al3xisvg.pythonanywhere.com/private/user/select';
     var urlShowResultados = 'http://al3xisvg.pythonanywhere.com/private/result/list';
     var __token = '';

	// Variables Globales:
	$scope.ngUsuario = {};
	$scope.ngResultados = [];

	/* Function: login
     * Arguments: Username/Contrasena
     * Output: $scope.usuario
     * Method: POST
     * Summary: Obtener token para acceder a las funciones de la api
     */
	$scope.login = function() {
		$http({
			url: urlLogin,
			method: 'POST',
			headers: {
						'Content-Type':'application/json'
					},
			data: 	{
						'Username':$scope.ngUsername,
						'Contrasena':$scope.ngContrasena
					}
		}).then(function(exito){
			if (exito.data['code']==201) {
                    __token = exito.data['data']['access_token'];
				$scope.showUsuario();
			} else {
                    __token = '';
                    $scope.ngUsername = '';
                    $scope.ngContrasena = '';
				orderHttpResponse(exito.data);	
			} 
		},function(error){
			console.log(error);
		});
	}

     /* Function: showUsuario
     * Arguments: Username
     * Output: Notify
     * Method: POST
     * Summary: Obtener datos del usuario
     */
     $scope.showUsuario = function() {
          $http({
               url: urlShowUsuario,
               method: 'POST',
               headers: {
                              'Content-Type':'application/json',
                              'DataToken': 'Bearer ' + __token
                         },
               data:     {
                              'Username':$scope.ngUsername
                         }
          }).then(function(exito){
               if (exito.data['code']==201) {
                    $scope.ngUsername = exito.data['data']['Username'];
                    $scope.ngContrasena = '';
                    $scope.ngUsuario = exito.data['data'];

                    Notify('Éxito!','success','Bienvenido estimado ' + exito.data['data']['Nombre']);
                    $scope.showResultados('2019-03-05', '2019-03-11', -1);
               } else {
                    __token = '';
                    $scope.ngUsername = '';
                    $scope.ngContrasena = '';
                    orderHttpResponse(exito.data);     
               } 
          }, function(error){
               console.log(error);
          });
     }

	/* Function: orderHttpResponse
     * Arguments: httpResponse
     * Output: 
     * Method: POST
     * Summary: Obtener datos de usuario al loggearse
     */
     function orderHttpResponse(http_response) {
          var err_war = ('error' in http_response)?'error': (('warning' in http_response)?'warning':'');
          switch(err_war) {
               case 'error':
                    Notify('Error!','error',http_response['error']);
                    break;
               case 'warning':
                    if (typeof http_response['warning']=='object') {
                         Notify('Advertencia!','warning',http_response['warning']['Error_Description']);
                    } else {
                         Notify('Advertencia!','warning',http_response['warning']);
                    }
                    break;
               default:   
                    Notify('Alerta!','warning',http_response['data']);
                    break;  
          }
     }

    /* Function: Notify (Error/Success)
     * Arguments: 
     * Output: -
     * Method: -
     * Summary: show a message with error in proccess
     */
     function Notify(title,tipo,msg){  
        new PNotify({ 
        			    title: title,
                        text: msg,
                        icon: 'fa fa-warning',
                        type: tipo,
                        styling: 'fontawesome'
                    });
     }

    /* Function: showInfoUbicYCampo 
     * Arguments: 
     * Output: [{}]
     * Method: -
     * Summary: show all data of ubicacion y campo
     */
     function showInfoUbicYCampo(){  
     	$http({
			url: urlInfoUbicYCampo,
			method: 'GET'
		}).then(function(exito){
			$scope.ngProvincias = exito.data['data'];
		},function(error){
			console.log(error);
		});
     }

     /* Function: showDistritosByProvincia
     * Arguments: idProvincia
     * Output: [{}]
     * Method: -
     * Summary: show all distritos by provincia selected
     */
     $scope.showDistritosByProvincia = function(id_provincia) {
     	var distritos;
     	$scope.ngProvincias.forEach(function(element){
     		if (element['idProvincia']==id_provincia) {
     			distritos = element['z_distritos'];
     		}
     	});
     	$scope.ngDistritos = distritos;
     }

     /* Function: showLocalidadByDistrito
     * Arguments: idDistrito
     * Output: [{}]
     * Method: -
     * Summary: show all distritos by provincia selected
     */
     $scope.showLocalidadesByDistrito = function(id_distrito) {
     	var localidades;
     	$scope.ngDistritos.forEach(function(element){
     		if (element['idDistrito']==id_distrito) {
     			localidades = element['z_localidades'];
     		}
     	});
     	$scope.ngLocalidades = localidades;
     }

     /* Function: showCultivosByLocalidad
     * Arguments: idLocalidad
     * Output: [{}]
     * Method: -
     * Summary: show all distritos by provincia selected
     */
     $scope.showCultivosByLocalidad = function(id_localidad) {
     	var cultivos;
     	$scope.ngLocalidades.forEach(function(element){
     		if (element['idLocalidad']==id_localidad) {
     			cultivos = element['z_cultivos'];
     		}
     	});
     	$scope.ngCultivos = cultivos;
     }

     /* Function: showCultivosByLocalidad
     * Arguments: idLocalidad
     * Output: [{}]
     * Method: -
     * Summary: show all distritos by provincia selected
     */
     $scope.showSurcosByCultivo = function(id_cultivo) {
     	var surcos;
     	$scope.ngCultivos.forEach(function(element){
     		if (element['idCultivo']==id_cultivo) {
     			surcos= element['z_surcos'];
     		}
     	});
     	$scope.ngSurcos = surcos;
     }

     /* Function: showResultados
     * Arguments: FecIni, FecFin, idUsuario, idCultivo
     * Output: [{}]
     * Method: -
     * Summary: show all resultados
     */
     $scope.showResultados = function(fec_ini, fec_fin, id_cultivo) {
          $scope.ngResultados = [];
          $http({
               url: urlShowResultados,
               method: 'POST',
               headers: {
                              'Content-Type':'application/json',
                              'DataToken': 'Bearer ' + __token
                         },
               data:     {
                              'FecIni': fec_ini,
                              'FecFin': fec_fin,
                              'idUsuario': $scope.ngUsuario['idUsuario'],
                              'idCultivo': id_cultivo,
                              'Username': $scope.ngUsername,
                              'Rol_id': $scope.ngUsuario['Rol_id']
                         }
          }).then(function(exito){
               if (exito.data['code']==201) {
                    $scope.ngResultados = exito.data['data'];
               } else {
                    orderHttpResponse(exito.data);     
               } 
          }, function(error){
               console.log(error);
          });
     }

     // Funciones al cargar la pagina:
     showInfoUbicYCampo();

});