var app = angular.module('home',[]);

app.controller('homeController',function($scope,$http,$window) {

	// ---- Funciones ----

	/* Function: showModal
     * Arguments: 
     * Output: -
     * Method: -
     * Summary: show init modal for login
     */
     function showModal() {
        $('#loginModal').modal('show');
     }

    /* Function: gotoLogin
     * Arguments: 
     * Output: -
     * Method: -
     * Summary: show init modal for login
     */
     $scope.gotoLoginView = function() {
        $window.location.href = '../html/login.html'
     }

	/* Function: Notify (Error/Success)
     * Arguments: 
     * Output: -
     * Method: -
     * Summary: show a message with error in proccess
     */
     function Notify(msg,tipo){
        new PNotify({ 
                        text: msg,
                        hide: true,
                        type: tipo,
                        styling: 'bootstrap3',
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });

     }

     // Funciones al cargar la pagina:
     showModal();

});