var app = angular.module('login',[]);

app.controller('loginController',function($scope,$http,$window) {

	// ---- Declaracion de Constantes ----
	loginUri = uriBase + '/auth/login';
	getDataUserUri = uriBase + '/private/user/sel';
	token = '';

	// ---- Declaracion de Variables ----
	$scope.ngUsuario = {};

	// ---- Funciones ----

	/* Function: showModal
     * Arguments: 
     * Output: -
     * Method: -
     * Summary: show init modal for login
     */
     $scope.login = function() {
      $http({
          url:loginUri,
          method:'POST',
          headers: {
              'Content-Type':'application/json'
          },
          data: {
              'Username': $scope.ngUsuario.Username,
              'Password': $scope.ngUsuario.Password
          },
      }).then(function(exito) {
        if(exito.data['code'] != 201) {
          $scope.ngUsuario.Username = '';
          $scope.ngUsuario.Password = '';
          Notify('Usuario o contrasena incorrectas ! ', 'warning'); 		
        } else {
          $scope.ngUsuario.Password = '';
      token = exito.data['data']['access_token'];
      $scope.getDataUser(token);
      $window.localStorage.setItem("current_token", token);
        }
      },function(error) {
        $scope.ngUsuario.Username = '';
        $scope.ngUsuario.Password = '';
          Notify('Ocurrio un error en el Login ! ', 'error');
      })
     }

	/* Function: Notify (Error/Success)
     * Arguments: 
     * Output: -
     * Method: -
     * Summary: show a message with error in proccess
     */
     function Notify(msg,tipo){
        new PNotify({ 
                        text: msg,
                        hide: true,
                        type: tipo,
                        styling: 'bootstrap3',
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });

     }

     // Funciones al cargar la pagina:
     showModal();

});