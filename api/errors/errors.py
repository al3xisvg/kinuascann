#!/usr/bin/env python3.6  
# -*- coding:utf-8 -*-   

from flask import jsonify

def error400(e):
    return jsonify({'error_code':400,'msg':str(e)})

def error401(e):
    return jsonify({'error_code':401,'msg':str(e)})

def error403(e):
    return jsonify({'error_code':403,'msg':str(e)})

def error404(e):
	return jsonify({'error_code':404,'msg':str(e)})

def error405(e):
	return jsonify({'error_code':405,'msg':str(e)})

def error409(e):
	return jsonify({'error_code':409,'msg':str(e)})

def error410(e):
	return jsonify({'error_code':410,'msg':str(e)})

def error500(e):
	return jsonify({'error_code':500,'msg':str(e)})

def error501(e):
	return jsonify({'error_code':501,'msg':str(e)})

def error502(e):
	return jsonify({'error_code':502,'msg':str(e)})

def error503(e):
	return jsonify({'error_code':503,'msg':str(e)})

def error504(e):
	return jsonify({'error_code':504,'msg':str(e)})