#!/usr/bin/env python3.6 
# -*- coding: utf-8 -*- 

def sanitizer(base=None, data=None, types=object):
    response = dict()
    flag_status = False
    response['attrs'] = {}
    response['not_types'] = None
    response['not_found'] = None
    try:
        attrs = dict( (k,v) for k,v in types.__dict__.items() if not k.startswith('__') )
        campos = []
        not_types = dict()
        not_found = []
        for k,v in attrs.items():
            for i in data:
                if k==i:
                    #data[i] = data[i].encode('utf-8') if isinstance(data[i],unicode) else data[i] # For python 2
                    if not isinstance(data[i],dict):
                        if v is not type(data[i]) and (not isinstance(v, Iterable) or type(data[i]) not in v):
                            not_types[k] = str(type(data[i]))
                        else:
                            base[k] = data[i] #base.append(data[i])
                            campos.append(k)
                    else:   # Mejorar para que busque los items dentro de dict que no se encuentran
                        dic_model = attrs[k]
                        dic_base = dict()
                        for key,value in data[i].items():
                            #value = value.encode('utf-8') if isinstance(value,unicode) else value # For python 2
                            if isinstance(value,dic_model[key]):
                                dic_base[key] = value
                                campos.append(k)
                            else:
                                not_types[k] = str(type(value))
                        base[k] = dic_base
        merge = dict()
        merge_base = base.copy() 
        merge_base.update(base)

        # Validacion de Items:
        if len(attrs)!=len(base):
            for k,v in attrs.items():
                if k not in campos:
                    not_found.append(k)

        flag_status = True if (len(not_types)==0 and len(not_found)==0) else False
        response['attrs'] = base
        response['not_types'] = not_types
        response['not_found'] = list(set(not_found)^set(list(not_types.keys())))

    except Exception as ex:
        flag_status = False
        response['error'] = str(ex)
    finally:
        return response, flag_status

def modelizer(base=None, data=None, types=object):
    merge = base
    attrs = dict( (k,v) for k,v in types.__dict__.items() if not k.startswith('__') )
    for k,v in attrs.items():
        if isinstance(merge,list):
            merge.append(data[k])
        else:
            merge[k] = data[k]
    return merge

def get_kwargs(rekuest, **args):
    try:
        return {'start': int(rekuest.args.get('start', app.config['PAGINATION_START'])),
                'limit': int(rekuest.args.get('limit', app.config['PAGINATION_LIMIT'])),
                'url_base': rekuest.base_url, 'args_sp': args} if rekuest else {'args_sp': args}
    except Exception:
        return None

def comparePasswords(password_bd, password_send):
    check = bcrypt.check_password_hash(password_bd, password_send)
    return check

def findElementBD(find_item, table, by_item, param, mult=False, various=False, format_time=False):
    out_item = None
    try:
        query_base = "select "+find_item+" from "+table+" where "+by_item+"="
        query_item =  str(param) if type(param)==int else "'"+str(param)+"'"
        motor = db.engine.execute(query_base+query_item)
        fun = motor.first() if mult==False else  motor.fetchall()
        fun = [fun] if mult==False else fun
        out = [f[0] for f in fun] if various==False else fun
        out_item = []
        for o in out: 
            fec = ( str(o) if format_time==True else (str(o).split())[0] ) if isinstance(o,datetime) else o
            out_item.append(fec)
        out_item = out_item[0] if mult==False else out_item
    except Exception as ex: 
        out_item = None
    finally:
        return out_item    

def updateElementBD(table, col_set, param_set, col_where, param_where):
    out_item = False
    try:
        param_set = param_set if isinstance(param_set,int) or isinstance(param_set,float) else "'"+str(param_set)+"'"
        param_where = param_where if isinstance(param_where,int) or isinstance(param_set,float) else "'"+str(param_where)+"'"
        query = "update "+table+" set "+col_set+"="+str(param_set)+" where "+col_where+"="+str(param_where)
        db.engine.execute(query)
        out_item = True
    except Exception as ex:
        out_item = False
    finally:
        return out_item

def insertElementBD(table,data_json):
    out_item = False
    try:
        fields = ""
        values = ""
        for k,v in data_json.iteritems():
            fields = fields+str(k)+","
            values = values+str(v)+"," if isinstance(v,int) or isinstance(v,float) else values+"'"+str(v)+"',"
        fields = fields[:-1]
        values = values[:-1]
        query = "insert into "+table+"("+fields+") values ("+values+")"
        db.engine.execute(query)
        #db.session.commit() (__init__ set autocommit)
        out_item = True
    except Exception as ex:
        #print 'ex---> ', str(ex)
        out_item = False
    finally:
        return out_item