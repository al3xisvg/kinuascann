#!/usr/bin/env python3.6 
# -*- coding: utf-8 -*- 

from apps import db
from decimal import Decimal
from datetime import date, datetime

def genericConnect(sp_name, list_data=[], pagination=False, mult=False, **kwargs):
	connect = db.engine
	connect_raw = connect.raw_connection()
	cur = connect_raw.cursor()
	formato_datetime = '%Y-%m-%d %H-%M:%S'
	formato_date = '%Y-%m-%d'
	response = None
	error = None
	try:

		# Get all data from store procedure:
		cur.callproc(sp_name, list_data)
		headers = [i[0] for i in cur.description] if cur.description else None
		resul = cur.fetchone() if mult is False else cur.fetchall()
		cur.nextset()
		connect_raw.commit()			

		# Validate output store:
		if not resul:
			cur.close()
			connect_raw.close()
			response = 'Changes made successfully.'
			return response, error

		# Make list of dictionaries	
		if mult is False:
			d = dict()
			for i,h in enumerate(headers):
				if isinstance(resul[i], Decimal):
					d[h] = float(resul[i])
				elif isinstance(resul[i], datetime):
					d[h] = resul[i].strftime(formato_datetime)
				elif isinstance(resul[i], date):
					d[h] = resul[i].strftime(formato_date) 
				elif resul[i] is None:
					d[h] = '--'
				else:
					d[h] = resul[i]
			response = d
		else:
			temp = []
			for r in resul:
				d = dict()
				for i,h in enumerate(headers):
					if isinstance(r[i], Decimal):
						d[h] = float(r[i])
					elif isinstance(r[i], datetime):
						d[h] = r[i].strftime(formato_datetime)
					elif isinstance(r[i], date):
						d[h] = r[i].strftime(formato_date) 
					elif r[i] is None:
						d[h] = '--'
					else:
						d[h] = r[i]
				temp.append(d)
			response = temp

		# Construccion de Paginado:
		if pagination is True:

			result = dict()
			result['count'] = len(response)
			result['limit'] = kwargs['limit']
			result['start'] = kwargs['start']
			result['next'] = None
			result['previous'] = None 
			
			url= kwargs['url_base']
			count = len(response)
			start = abs(kwargs['start'])
			limit = abs(kwargs['limit'])

			if start == 1:
				result['previous'] = None
			else:
				start_copy = max(1, start - limit)
				limit_copy = start - 1
				result['previous'] = url + '?start={}&limit={}'.format(start_copy, limit)

			if start + limit > count:
				result['next'] = None
			else:
				start_copy = start + limit
				result['next'] = url + '?start={}&limit={}'.format(start_copy, limit)

			result['result'] = response[(start-1):(start-1+limit)]
			response = result

	except Exception as ex:
		error = str(ex)
	finally:
		cur.close()
		connect_raw.close()
		return response, error