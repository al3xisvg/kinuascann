#!/usr/bin/env python3.6  
# -*- coding:utf-8 -*-   

from flask import Flask  
from flask_sqlalchemy import SQLAlchemy   
from flask_jwt_extended import JWTManager 
from flask_bcrypt import Bcrypt 
from flask_cors import CORS
from errors.errors import *
import os

# Aplicacion:  
app = Flask(__name__) # template_folder=BASE_DIR+'templates'    
app_settings = os.getenv('APP_SETTINGS','apps.config.DevelopmentConfig')  
app.config.from_object(app_settings)   

# Conexion MySQL:
db = SQLAlchemy(app, session_options={'autocommit':True}) 

# Token:  
jwt = JWTManager(app)   

# Encriptacion:
bcrypt = Bcrypt(app)

# Cors:
CORS(app)

# Manejo de errores:
app.register_error_handler(400,error400)
app.register_error_handler(401,error401)
app.register_error_handler(403,error403)
app.register_error_handler(404,error404)
app.register_error_handler(405,error405)
app.register_error_handler(409,error409)
app.register_error_handler(410,error410)
app.register_error_handler(500,error500)
app.register_error_handler(501,error501)
app.register_error_handler(502,error502)
app.register_error_handler(503,error503)
app.register_error_handler(504,error504)

# Blueprint: 
from apps.auth import auth_server  
from apps.private import endpoints_privates
from apps.public import endpoints_publics

app.register_blueprint(auth_server, url_prefix='/auth')   
app.register_blueprint(endpoints_privates, url_prefix='/private') 
app.register_blueprint(endpoints_publics, url_prefix='/public')     