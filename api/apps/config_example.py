#!/usr/bin/env python3.6   
# -*- coding: utf-8 -*-  

from datetime import timedelta  
import os   

class BaseConfig(object):   

    # Ruta:  
    BASE_DIR = '/home/al3xisvg/TESIS/kinuascann/api/' 

    # Run: 
    DEBUG = False   

    # Database:   
    DB_USERNAME = 'al3xisvg'   
    DB_PASSWORD = 'Lima2017.'    
    DB_HOST = 'al3xisvg.mysql.pythonanywhere-services.com'  
    DB_NAME = 'al3xisvg$HZ'    
    DB_DIALECT = 'mysql'   
    DB_DRIVER = 'pymysql'   

    # SQLAlchemy:  
    SQLALCHEMY_DATABASE_URI = '{0}+{1}://{2}:{3}@{4}/{5}'.format(DB_DIALECT, DB_DRIVER, DB_USERNAME, DB_PASSWORD, DB_HOST, DB_NAME)   
    SQLALCHEMY_TRACK_MODIFICATIONS = False   
    SQLALCHEMY_POOL_SIZE = 5  
    SQLALCHEMY_POOL_RECYCLE = 280   

    # JWT:   
    JWT_SECRET_KEY = 'Bitinka@0926'    
    JWT_HEADER_NAME = 'DataToken'  # {Authorization: Bearer --token--}   
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(seconds=3600)   

    # BCRYPT:   
    BCRYPT_LOG_ROUNDS = 13 

    # PAGINATION:  
    PAGINATION_START = 0
    PAGINATION_LIMIT = 10  

class DevelopmentConfig(BaseConfig):   
    DEVELOPMENT = True
    DEBUG = True 

class TestingConfig(BaseConfig):   
    TESTING = True   

class ProductionConfig(BaseConfig):   
    DEBUG = False   