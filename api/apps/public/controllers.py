#!/usr/bin/env python3.6
# -*- coding:utf-8 -*-

from .models import InsertUser, ListUser
from utils.connection import genericConnect
from utils.libs import sanitizer, modelizer
from flask_jwt_extended import jwt_required, jwt_optional, get_jwt_identity
from flask import request
from flask import jsonify
from flask_cors import cross_origin
import pandas as pd
import json

# (1)
@cross_origin()
def addUser():
	params = dict()
	custom_res = dict()
	custom_res['data'] = ''
	custom_res['code'] = 200
	custom_res['flag'] = False
	try:

		# Validacion de json request:
		data = request.json
		if not data:
			custom_res['warning'] = dict(msg='JSON Error')
			custom_res['code'] = 100
			return jsonify(custom_res)

		# Verificacion de datos:
		data_snt, flag_status = sanitizer(base=params, data=data, types=InsertUser)
		if flag_status is False:
			if 'error' in data_snt:
				custom_res['warning'] = dict(function='__sanitizer__',msg=data_snt['error'])
				custom_res['code'] = 101
			else:
				custom_res['warning'] = data_snt
				custom_res['code'] = 102
			return jsonify(custom_res)

		# Modelamiento de datos:
		merge = data_snt['attrs']
		merge_model = modelizer(base=[], data=merge, types=InsertUser)

		# Consultas a la BD mediante SPs:
		result, error = genericConnect(sp_name='sp_usuario_insertar', list_data=merge_model, pagination=False, mult=False)
		if error:
			custom_res['warning'] = dict(function='__genericConnect__',msg=error)
			custom_res['code'] = 103
			return jsonify(custom_res)
		if 'Error' in result:
			custom_res['warning'] = dict(store='sp_usuario_insertar')
			custom_res['warning'].update(result)
			return jsonify(custom_res)

		# Respuesta Final:
		custom_res['data'] = result
		custom_res['code'] = 201
		custom_res['flag'] = True

	except Exception as ex:
		custom_res['error'] = str(ex)
	finally:
		return jsonify(custom_res)

# (2)
@cross_origin()
def listUser():
	params = dict()
	custom_res = dict()
	custom_res['data'] = ''
	custom_res['code'] = 200
	custom_res['flag'] = False
	try:

		# Validacion de json request:
		data = request.json
		if not data:
			custom_res['warning'] = dict(msg='JSON Error')
			custom_res['code'] = 100
			return jsonify(custom_res)

		# Verificacion de datos:
		data_snt, flag_status = sanitizer(base=params, data=data, types=ListUser)
		if flag_status is False:
			if 'error' in data_snt:
				custom_res['warning'] = dict(function='__sanitizer__',msg=data_snt['error'])
				custom_res['code'] = 101
			else:
				custom_res['warning'] = data_snt
				custom_res['code'] = 102
			return jsonify(custom_res)

		# Modelamiento de datos:
		merge = data_snt['attrs']
		merge_model = modelizer(base=[], data=merge, types=ListUser)

		# Consultas a la BD mediante SPs:
		result, error = genericConnect(sp_name='sp_usuario_listar', list_data=merge_model, pagination=False, mult=True)
		if error:
			custom_res['warning'] = dict(function='__genericConnect__',msg=error)
			custom_res['code'] = 103
			return jsonify(custom_res)
		if 'Error' in result:
			custom_res['warning'] = dict(store='sp_usuario_listar')
			custom_res['warning'].update(result)
			return jsonify(custom_res)

		# Respuesta Final:
		custom_res['data'] = result
		custom_res['code'] = 201
		custom_res['flag'] = True

	except Exception as ex:
		custom_res['error'] = str(ex)
	finally:
		return jsonify(custom_res)

# (3)
@cross_origin()
def infoUbicCamp():
	params = dict()
	custom_res = dict()
	custom_res['data'] = ''
	custom_res['code'] = 200
	custom_res['flag'] = False
	try:

		# Consultas a la BD mediante SPs:
		res, error = genericConnect(sp_name='sp_info_ubicacionycampo', list_data=[], pagination=False, mult=True)
		if error:
			custom_res['warning'] = dict(function='__genericConnect__',msg=error)
			custom_res['code'] = 103
			return jsonify(custom_res)
		if 'Error' in res:
			custom_res['warning'] = dict(store='sp_info_ubicacionycampo')
			custom_res['warning'].update(res)
			return jsonify(custom_res)

		# Ordenamiento de data:
		resultado = []
		for r in res:
			resultado.append(list(r.values()))
		campos = ['idProvincia','Codigo','Nombre','Descripcion','idDistrito','Codigo','Nombre','Descripcion','idLocalidad','Nombre','Ubicacion','Descripcion','idCultivo','Codigo','Ancho','Largo','Perimetro','Institucion','Descripcion','idSurco','Codigo','Longitud']
		df = pd.DataFrame(resultado,columns=campos)

		# Ids:
		ids_provincias = df.iloc[:,0]
		ids_distritos = df.iloc[:,4]
		ids_localidades = df.iloc[:,8]
		ids_cultivos = df.iloc[:,12]
		ids_surcos = df.iloc[:,19]

		# Provincias:
		rows_provincias = (df.iloc[:,0:3]).drop_duplicates()
		stmt_provincias = rows_provincias.to_json(orient='records')
		json_provincias = json.loads(stmt_provincias)

		# Distritos:
		for p in json_provincias:
		    distritos = pd.concat([ids_provincias,df.iloc[:,4:7]], axis=1, join='inner')
		    rows_distritos = (distritos.loc[distritos['idProvincia']==p['idProvincia']]).drop_duplicates()
		    stmt_distritos = rows_distritos.to_json(orient='records')
		    json_distritos = json.loads(stmt_distritos)
		    p['z_distritos'] = json_distritos

		    # Localidades:
		    for d in json_distritos:
		    	localidades = pd.concat([ids_distritos,df.iloc[:,8:11]], axis=1, join='inner')
		    	rows_localidades = (localidades.loc[localidades['idDistrito']==d['idDistrito']]).drop_duplicates()
		    	stmt_localidades = rows_localidades.to_json(orient='records')
		    	json_localidades = json.loads(stmt_localidades)
		    	d['z_localidades'] = json_localidades

		    	# Cultivos:
		    	for l in json_localidades:
		    	    cultivos = pd.concat([ids_localidades,df.iloc[:,12:18]], axis=1, join='inner')
		    	    rows_cultivos = (cultivos.loc[cultivos['idLocalidad']==l['idLocalidad']]).drop_duplicates()
		    	    stmt_cultivos = rows_cultivos.to_json(orient='records')
		    	    json_cultivos = json.loads(stmt_cultivos)
		    	    l['z_cultivos'] = json_cultivos

		    	    # Surcos:
		    	    for c in json_cultivos:
		    	        surcos = pd.concat([ids_cultivos,df.iloc[:,19:21]], axis=1, join='inner').drop_duplicates()
		    	        rows_surcos = (surcos.loc[surcos['idCultivo']==c['idCultivo']]).drop_duplicates()
		    	        stmt_surcos = rows_surcos.to_json(orient='records')
		    	        json_surcos = json.loads(stmt_surcos)
		    	        c['z_surcos'] = json_surcos

		# Aglomerado:
		result = json_provincias

		# Respuesta Final:
		custom_res['data'] = result
		custom_res['code'] = 201
		custom_res['flag'] = True

	except Exception as ex:
		custom_res['error'] = str(ex)
	finally:
		return jsonify(custom_res)