#!/usr/bin/env python3.6  
# -*- coding: utf-8 -*-   

from flask import Blueprint

endpoints_publics = Blueprint('public',__name__)

from apps.public import routes