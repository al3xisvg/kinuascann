#!/usr/bin/env python3.6  
# -*- coding: utf-8 -*- 

from apps.public import endpoints_publics
from .controllers import addUser, listUser, infoUbicCamp

# (1) Crear Usuario:  
endpoints_publics.add_url_rule('/user/create', view_func=addUser, methods=['POST'])   

# (2) Listar Usuarios:  
endpoints_publics.add_url_rule('/user/list', view_func=listUser, methods=['POST'])   

# (3) Obtener informacion de la ubicacion y el cultivo:
endpoints_publics.add_url_rule('/info/ubicycamp', view_func=infoUbicCamp, methods=['GET'])   
