#!/usr/bin/env python3.6  
# -*- coding: utf-8 -*- 

from apps.auth import auth_server 
from .controllers import login   

# (1) Login:  
auth_server.add_url_rule('/login', view_func=login, methods=['POST'])   
