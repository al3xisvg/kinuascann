#!/usr/bin/env python3.6  
# -*- coding:utf-8 -*-   

from .models import Login  
from utils.connection import genericConnect
from utils.libs import sanitizer, modelizer
from flask_jwt_extended import create_access_token, create_refresh_token
from flask import request  
from flask import jsonify  
from flask_cors import cross_origin 

@cross_origin()
def login():
	params = dict()
	custom_res = dict()
	custom_res['data'] = ''
	custom_res['code'] = 200
	custom_res['flag'] = False
	try:
		# Validacion de json request:
		data = request.json
		if not data:
			custom_res['warning'] = dict(msg='JSON Error')
			custom_res['code'] = 100
			return jsonify(custom_res)

		# Verificacion de datos:
		data_snt, flag_status = sanitizer(base=params, data=data, types=Login)
		if flag_status is False:
			if 'error' in data_snt:
				custom_res['warning'] = dict(function='__sanitizer__',msg=data_snt['error'])
				custom_res['code'] = 101
			else:
				custom_res['warning'] = data_snt
				custom_res['code'] = 102
			return jsonify(custom_res)

		# Modelamiento de datos:
		merge = data_snt['attrs']
		merge_model = modelizer(base=[], data=merge, types=Login)

		# Consultas a la BD mediante SPs:
		result, error = genericConnect(sp_name='sp_usuario_validar', list_data=merge_model, pagination=False, mult=False)
		if error:
			custom_res['warning'] = dict(function='__genericConnect__',msg=error)
			custom_res['code'] = 103
			return jsonify(custom_res)
		if 'Error' in result:
			custom_res['warning'] = dict(store='sp_usuario_validar')
			custom_res['warning'].update(result)
			return jsonify(custom_res)

		# Validacion de credenciales de usuario:
		if result['Output'] == 0:
			custom_res['data'] = 'Username o Contrasena incorrectas.'
			custom_res['code'] = 104
			custom_res['flag'] = True
			return jsonify(custom_res)
			
		# Generacion de Token:
		msg = {
            'access_token': create_access_token(identity=merge['Username']),
            'refresh_token': create_refresh_token(identity=merge['Username'])
        }

		# Respuesta Final:
		custom_res['data'] = msg
		custom_res['code'] = 201
		custom_res['flag'] = True

	except Exception as ex:
		custom_res['error'] = str(ex)
	finally:
		return jsonify(custom_res)

