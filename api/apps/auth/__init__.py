#!/usr/bin/env python3.6  
# -*- coding: utf-8 -*-   

from flask import Blueprint

auth_server = Blueprint('auth',__name__)

from apps.auth import routes