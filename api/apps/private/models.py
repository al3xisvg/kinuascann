#!/usr/bin/env python3.6  
# -*- coding:utf-8 -*-   

class UpdateUser(object):
	idUsuario = int
	Nombre = str
	Paterno = str
	Materno = str
	DNI = str
	FecNac = str
	Email = str
	Localidad_id = int 
	Username = str
	Contrasena = str
	Rol_id = int

class SuspendUser(object):
	idUsuario = int

class SelectUser(object):
	Username = str

class ListResult(object):
	FecIni = str
	FecFin = str
	idUsuario = int
	idCultivo = int

class InsertResult(object):
	Vegetacion = float
	Amarilleamiento = float
	idUsuario = int
	idSurco = int
	idCultivo = int

class DeleteResult(object):
	Ids_Resultados = str