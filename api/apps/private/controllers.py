#!/usr/bin/env python3.6  
# -*- coding:utf-8 -*-   

from .models import UpdateUser, SuspendUser, SelectUser, ListResult, InsertResult, DeleteResult
from utils.connection import genericConnect
from utils.libs import sanitizer, modelizer
from flask_jwt_extended import jwt_required, jwt_optional, get_jwt_identity
from flask import request  
from flask import jsonify  
from apps import app
from flask_cors import cross_origin

# (1)
@cross_origin()
@jwt_required
def updUser():
	params = dict()
	custom_res = dict()
	custom_res['data'] = ''
	custom_res['code'] = 200
	custom_res['flag'] = False
	try:

		# Validacion de json request:
		data = request.json
		if not data:
			custom_res['warning'] = dict(msg='JSON Error')
			custom_res['code'] = 100
			return jsonify(custom_res)

		# Verificacion de datos:
		data_snt, flag_status = sanitizer(base=params, data=data, types=UpdateUser)
		if flag_status is False:
			if 'error' in data_snt:
				custom_res['warning'] = dict(function='__sanitizer__',msg=data_snt['error'])
				custom_res['code'] = 101
			else:
				custom_res['warning'] = data_snt
				custom_res['code'] = 102
			return jsonify(custom_res)

		# Validacion de sesion de usuario:
		if not data_snt['attrs']['Rol_id'] == 1: 	# Usuario con Rol Administrador
			if not data_snt['attrs']['Username'] == get_jwt_identity():
				custom_res['warning'] = 'Usuario diferente a la sesion.'
				custom_res['code'] = 103
				return jsonify(custom_res)

		# Modelamiento de datos:
		merge = data_snt['attrs']
		merge_model = modelizer(base=[], data=merge, types=UpdateUser)

		# Consultas a la BD mediante SPs:
		result, error = genericConnect(sp_name='sp_usuario_update', list_data=merge_model, pagination=False, mult=False)
		if error:
			custom_res['warning'] = dict(function='__genericConnect__',msg=error)
			custom_res['code'] = 103
			return jsonify(custom_res)
		if 'Error' in result:
			custom_res['warning'] = dict(store='sp_usuario_update')
			custom_res['warning'].update(result)
			return jsonify(custom_res)

		# Respuesta Final:
		custom_res['data'] = result
		custom_res['code'] = 201
		custom_res['flag'] = True

	except Exception as ex:
		custom_res['error'] = str(ex)
	finally:
		return jsonify(custom_res)

# (2)
@cross_origin()
@jwt_required
def delUser():
	params = dict()
	custom_res = dict()
	custom_res['data'] = ''
	custom_res['code'] = 200
	custom_res['flag'] = False
	try:
		# Validacion de json request:
		data = request.json
		if not data:
			custom_res['warning'] = dict(msg='JSON Error')
			custom_res['code'] = 100
			return jsonify(custom_res)

		# Verificacion de datos:
		data_snt, flag_status = sanitizer(base=params, data=data, types=SuspendUser)
		if flag_status is False:
			if 'error' in data_snt:
				custom_res['warning'] = dict(function='__sanitizer__',msg=data_snt['error'])
				custom_res['code'] = 101
			else:
				custom_res['warning'] = data_snt
				custom_res['code'] = 102
			return jsonify(custom_res)

		# Validacion de sesion de usuario:
		if not data['Rol_id'] == 1: 	# Usuario con Rol Administrador
			if not data['Username'] == get_jwt_identity():
				custom_res['warning'] = 'Usuario diferente a la sesion.'
				custom_res['code'] = 103
				return jsonify(custom_res)

		# Modelamiento de datos:
		merge = data_snt['attrs']
		merge_model = modelizer(base=[], data=merge, types=SuspendUser)

		# Consultas a la BD mediante SPs:
		result, error = genericConnect(sp_name='sp_usuario_suspender', list_data=merge_model, pagination=False, mult=False)
		if error:
			custom_res['warning'] = dict(function='__genericConnect__',msg=error)
			custom_res['code'] = 103
			return jsonify(custom_res)
		if 'Error' in result:
			custom_res['warning'] = dict(store='sp_usuario_suspender')
			custom_res['warning'].update(result)
			return jsonify(custom_res)
		
		# Respuesta Final:
		custom_res['data'] = result
		custom_res['code'] = 201
		custom_res['flag'] = True

	except Exception as ex:
		custom_res['error'] = str(ex)
	finally:
		return jsonify(custom_res)

# (3)
@cross_origin()
@jwt_required
def selUser():
	params = dict()
	custom_res = dict()
	custom_res['data'] = ''
	custom_res['code'] = 200
	custom_res['flag'] = False
	try:
		# Validacion de json request:
		data = request.json
		if not data:
			custom_res['warning'] = dict(msg='JSON Error')
			custom_res['code'] = 100
			return jsonify(custom_res)

		# Verificacion de datos:
		data_snt, flag_status = sanitizer(base=params, data=data, types=SelectUser)
		if flag_status is False:
			if 'error' in data_snt:
				custom_res['warning'] = dict(function='__sanitizer__',msg=data_snt['error'])
				custom_res['code'] = 101
			else:
				custom_res['warning'] = data_snt
				custom_res['code'] = 102
			return jsonify(custom_res)

		# Validacion de sesion de usuario:
		if not data['Username'] == get_jwt_identity():
			custom_res['warning'] = 'Usuario diferente a la sesion.'
			custom_res['code'] = 103
			return jsonify(custom_res)

		# Modelamiento de datos:
		merge = data_snt['attrs']
		merge_model = modelizer(base=[], data=merge, types=SelectUser)

		# Consultas a la BD mediante SPs:
		result, error = genericConnect(sp_name='sp_usuario_obtener', list_data=merge_model, pagination=False, mult=False)
		if error:
			custom_res['warning'] = dict(function='__genericConnect__',msg=error)
			custom_res['code'] = 103
			return jsonify(custom_res)
		if 'Error' in result:
			custom_res['warning'] = dict(store='sp_usuario_obtener')
			custom_res['warning'].update(result)
			return jsonify(custom_res)
		
		# Respuesta Final:
		custom_res['data'] = result
		custom_res['code'] = 201
		custom_res['flag'] = True

	except Exception as ex:
		custom_res['error'] = str(ex)
	finally:
		return jsonify(custom_res)

# (4)
@cross_origin()
@jwt_required
def listResult():
	params = dict()
	custom_res = dict()
	custom_res['data'] = ''
	custom_res['code'] = 200
	custom_res['flag'] = False
	try:

		# Validacion de json request:
		data = request.json
		if not data:
			custom_res['warning'] = dict(msg='JSON Error')
			custom_res['code'] = 100
			return jsonify(custom_res)

		# Verificacion de datos:
		data_snt, flag_status = sanitizer(base=params, data=data, types=ListResult)
		if flag_status is False:
			if 'error' in data_snt:
				custom_res['warning'] = dict(function='__sanitizer__',msg=data_snt['error'])
				custom_res['code'] = 101
			else:
				custom_res['warning'] = data_snt
				custom_res['code'] = 102
			return jsonify(custom_res)

		# Validacion de sesion de usuario:
		if not data['Rol_id'] == 1: 	# Usuario con Rol Administrador
			if not data['Username'] == get_jwt_identity():
				custom_res['warning'] = 'Usuario diferente a la sesion.'
				custom_res['code'] = 103
				return jsonify(custom_res)

		# Modelamiento de datos:
		merge = data_snt['attrs']
		merge_model = modelizer(base=[], data=merge, types=ListResult)

		# Agregado de Paginado - Inicio, Rango:
		p_start = app.config['PAGINATION_START']
		p_range = app.config['PAGINATION_LIMIT']
		merge_model.append(p_start)
		merge_model.append(p_range)

		# Consultas a la BD mediante SPs:
		result, error = genericConnect(sp_name='sp_resultado_listar', list_data=merge_model, pagination=False, mult=True)
		if error:
			custom_res['warning'] = dict(function='__genericConnect__',msg=error)
			custom_res['code'] = 103
			return jsonify(custom_res)
		if 'Error' in result:
			custom_res['warning'] = dict(store='sp_resultado_listar')
			custom_res['warning'].update(result)
			return jsonify(custom_res)

		# Respuesta Final:
		custom_res['data'] = result
		custom_res['code'] = 201
		custom_res['flag'] = True

	except Exception as ex:
		custom_res['error'] = str(ex)
	finally:
		return jsonify(custom_res)

# (5) 
@cross_origin()
@jwt_required
def addResult():
	params = dict()
	custom_res = dict()
	custom_res['data'] = ''
	custom_res['code'] = 200
	custom_res['flag'] = False
	try:
		
		# Validacion de json request:
		data = request.json
		if not data:
			custom_res['warning'] = dict(msg='JSON Error')
			custom_res['code'] = 100
			return jsonify(custom_res)

		# Verificacion de datos:
		data_snt, flag_status = sanitizer(base=params, data=data, types=InsertResult)
		if flag_status is False:
			if 'error' in data_snt:
				custom_res['warning'] = dict(function='__sanitizer__',msg=data_snt['error'])
				custom_res['code'] = 101
			else:
				custom_res['warning'] = data_snt
				custom_res['code'] = 102
			return jsonify(custom_res)

		# Validacion de sesion de usuario:
		if not data['Rol_id'] == 1: 	# Usuario con Rol Administrador
			if not data['Username'] == get_jwt_identity():
				custom_res['warning'] = 'Usuario diferente a la sesion.'
				custom_res['code'] = 103
				return jsonify(custom_res)

		# Modelamiento de datos:
		merge = data_snt['attrs']
		merge_model = modelizer(base=[], data=merge, types=InsertResult)

		# Consultas a la BD mediante SPs:
		result, error = genericConnect(sp_name='sp_resultado_insertar', list_data=merge_model, pagination=False, mult=False)
		if error:
			custom_res['warning'] = dict(function='__genericConnect__',msg=error)
			custom_res['code'] = 103
			return jsonify(custom_res)
		if 'Error' in result:
			custom_res['warning'] = dict(store='sp_resultado_insertar')
			custom_res['warning'].update(result)
			return jsonify(custom_res)

		# Respuesta Final:
		custom_res['data'] = result
		custom_res['code'] = 201
		custom_res['flag'] = True

	except Exception as ex:
		custom_res['error'] = str(ex)
	finally:
		return jsonify(custom_res)

# (6) 
@cross_origin()
@jwt_required
def delResult():
	params = dict()
	custom_res = dict()
	custom_res['data'] = ''
	custom_res['code'] = 200
	custom_res['flag'] = False
	try:

		# Validacion de json request:
		data = request.json
		if not data:
			custom_res['warning'] = dict(msg='JSON Error')
			custom_res['code'] = 100
			return jsonify(custom_res)

		# Verificacion de datos:
		data_snt, flag_status = sanitizer(base=params, data=data, types=DeleteResult)
		if flag_status is False:
			if 'error' in data_snt:
				custom_res['warning'] = dict(function='__sanitizer__',msg=data_snt['error'])
				custom_res['code'] = 101
			else:
				custom_res['warning'] = data_snt
				custom_res['code'] = 102
			return jsonify(custom_res)

		# Validacion de sesion de usuario:
		if not data['Rol_id'] == 1: 	# Usuario con Rol Administrador
			if not data['Username'] == get_jwt_identity():
				custom_res['warning'] = 'Usuario diferente a la sesion.'
				custom_res['code'] = 103
				return jsonify(custom_res)

		# Modelamiento de datos:
		merge = data_snt['attrs']
		merge_model = modelizer(base=[], data=merge, types=DeleteResult)

		# Consultas a la BD mediante SPs:
		result, error = genericConnect(sp_name='sp_resultado_eliminar', list_data=merge_model, pagination=False, mult=False)
		if error:
			custom_res['warning'] = dict(function='__genericConnect__',msg=error)
			custom_res['code'] = 103
			return jsonify(custom_res)
		if 'Error' in result:
			custom_res['warning'] = dict(store='sp_resultado_eliminar')
			custom_res['warning'].update(result)
			return jsonify(custom_res)

		# Respuesta Final:
		custom_res['data'] = result
		custom_res['code'] = 201
		custom_res['flag'] = True

	except Exception as ex:
		custom_res['error'] = str(ex)
	finally:
		return jsonify(custom_res)