#!/usr/bin/env python3.6  
# -*- coding: utf-8 -*-   

from flask import Blueprint

endpoints_privates = Blueprint('private',__name__)

from apps.private import routes