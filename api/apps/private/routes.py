#!/usr/bin/env python3.6  
# -*- coding: utf-8 -*- 

from apps.private import endpoints_privates
from .controllers import updUser, delUser, selUser, listResult, addResult, delResult

# (1) Actualizar Usuario:  
endpoints_privates.add_url_rule('/user/update', view_func=updUser, methods=['POST'])   

# (2) Suspender Usuario: 
endpoints_privates.add_url_rule('/user/suspend', view_func=delUser, methods=['POST'])   

# (3) Obtener Usuario:
endpoints_privates.add_url_rule('/user/select', view_func=selUser, methods=['POST'])   

# (4) Listar Resultados:  
endpoints_privates.add_url_rule('/result/list', view_func=listResult, methods=['POST'])   

# (5) Insertar Resultados:  
endpoints_privates.add_url_rule('/result/add', view_func=addResult, methods=['POST'])   

# (6) Eliminar Resultados:  
endpoints_privates.add_url_rule('/result/del', view_func=delResult, methods=['POST']) 