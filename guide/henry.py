#! /usr/bin/python2.7   
# -*- coding: utf-8 -*-   

import Tkinter as tk  
#import RPi.GPIO as GPIO   
import commands
import cv2
import os
import logging
from PIL import Image, ImageTk
from datetime import datetime
from fcn_procesamiento import procesamiento_vegetacion
from fcn_procesamiento import procesamiento_amarillo

from tkintertable import TableCanvas, TableModel
import json
import csv
import tablib

"""
Class: Variables   
Tipo: Declaracion  
To: system   
"""  
PATH = commands.getoutput('pwd') + '/'   
PATH_ICONS = PATH + 'icons/'
PATH_CAPTURES = PATH + 'captures/'
PATH_TEMPS = PATH + 'temps/'
PATH_EXPORTS = PATH + 'exports/'
ruta_img_orig = PATH_TEMPS + 'original.png'
ruta_img_veget = PATH_TEMPS + 'r1_vegetacion.png'
ruta_img_amari = PATH_TEMPS + 'r2_amarilleamiento.png'

"""
Clase: Variables  
Tipo: Declaracion  
To: tkinter  
"""
root = None  
FrameSizeX = 720   
FrameSizeY = 470   
COLOR_FRAME = "#766C99"  
COLOR_BTN = "#7DC0AE"   
COLOR_ACTIVE_BTN = "#3C5181"
COLOR_BTN_HOME = "#F64876"
canvas_width = 50   
canvas_height = 50  
radius = 15    

"""
Clase: Variables  
Tipo: Declaracion  
To: RPi.GPIO     
"""
"""
PIN_PushBtn_Before = 3
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN_PushBtn_Before, GPIO.IN)

PIN_PushBtn_Center = 2
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN_PushBtn_Center, GPIO.IN) 

PIN_PushBtn_Next = 4  
GPIO.setmode(GPIO.BCM)    
GPIO.setup(PIN_PushBtn_Next, GPIO.IN)    
"""

"""
Clase: Flags
Tipo: Booleanos
To: Frames 
"""
name_frame = None 

"""
Clase: PiCamera
Tipo: Driver
To: Videocaptura
"""
commands.getoutput('sudo modprobe bcm2835-v4l2')

"""
Clase: Flags
Tipo: Booleanos
To: Camara
"""
flag_captura = False
flag_procesar = True

"""
Clase: Flags
Tipo: Numeric
To: Type of Save
"""
n_saver = 1

"""
Clase: Dict
Tipo: Object
To: Resultados
"""
data_resultados = dict()

"""
Clase: Config
Tipo: Sistema
To: Logs
"""
formatter = logging.Formatter("%(asctime)s %(levelname)s %(message)s","%Y-%m-%d %H:%M:%S")
logger = logging.getLogger()
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
ch.setFormatter(formatter)
logger.addHandler(ch)

class formato_botones(tk.Frame):
    def __init__(self, master=None, **kwargs):
        tk.Frame.__init__(self, master)
        self.rowconfigure(0, minsize=kwargs.pop('height',None))
        self.columnconfigure(0, minsize=kwargs.pop('width',None))
        self.btn = tk.Button(self, **kwargs)
        self.btn.grid(row=0, column=0, sticky="nsew")
        self.config = self.btn.config

class GuideTk(tk.Tk):    
    def __init__(self, *args, **kwargs):   

        # Definicion - Tk() Object:  
        tk.Tk.__init__(self)   
        self.title('KinuaScann 2017 v2.0')     

        # Root:  
        global root  
        root = self 

        # Ubicacion y Tamaño del Tk() Object:  
        ScreenSizeX = self.winfo_screenwidth()   
        ScreenSizeY = self.winfo_screenheight() 
        ScreenRatio = 0.8   
        FramePosX = (ScreenSizeX - FrameSizeX)/2   
        FramePosY = (ScreenSizeY - FrameSizeY)/2    
        self.geometry('%dx%d+%d+%d' % (FrameSizeX,FrameSizeY,FramePosX,FramePosY))      

        # Creacion de Componentes:  
        self.create_widgets()   

    def create_widgets(self):    

        # Contenedor Tk():  
        self.container = tk.Frame(root)   
        self.container.grid(row=0, column=0, sticky=tk.W + tk.E)  

        # Inicializacion de Ventanas:   
        self.frames = {}    
        self.pages = [P6_ResultsNumber, P7_SaveResponse]
        for F in self.pages:   
            frame = F(self.container, self, self.pages)    
            self.frames[F] = frame  
        
        # Visualizacion inicial:
        logger.info('[SYSTEM-START] Bienvenido a KinuaScann')
        self.show_frame(P6_ResultsNumber)

    def show_frame(self, page_name):
        global name_frame
        name_frame = (str(page_name)).replace('__main__.','')
        logger.info('[WINDOW] Pass to Frame ' + str(name_frame))
        frame = self.frames[page_name]    
        frame.tkraise()   

class BaseFrame(tk.Frame):
    def __init__(self, parent, controller, pages):   

        # Definicion - Frame() Object:      
        tk.Frame.__init__(self, parent)   
        self.parent = parent   
        self.controller = controller  
        self.pages = pages   

        # Caracteristicas de Frame:  
        self.grid(row=0, column=0, sticky=tk.W + tk.E)   
        self.config(width=720, height=470,bg=COLOR_FRAME)    
        self.grid_propagate(False)       

        # Crear componentes:    
        self.create_widgets()   

        # Canvas:
        self.canvas = tk.Canvas(self, width=canvas_width, height=canvas_height)
        self.canvas.config(bg=COLOR_FRAME, highlightbackground=COLOR_FRAME)
        x0 = (canvas_width/2) - radius
        y0 = (canvas_height/2) - radius
        x1 = (canvas_width/2) + radius
        y1 = (canvas_height/2) + radius
        self.circle = self.canvas.create_oval(x0, y0, x1, y1, width=2, fill="black")
        self.canvas.place(relx=0.93, rely=0.0)

    def create_widgets(self):    
        raise NotImplementedError  

def Table(self, data):
    # Modelo:
    table_header = ['Parcela', 'Vegetacion', 'Amarilleamiento']
    model = TableModel()
    for column in table_header: 
      model.addColumn(column)
    model.importDict(data)

    # Tabla:
    table = TableCanvas(self, model=model,cellbackgr="#766C99" ,rowselectedcolor='yellow',editable=False) #, data=data)
    table.show()
    
class P6_ResultsNumber(BaseFrame):
    def create_widgets(self):
        # Data Simulate:
        percent_surco1 = ['99.7 %', '100.1 %']
        percent_surco2 = ['22.7 %', '87.1 %']
        percent_surco3 = ['22.7 %', '87.1 %']
        percent_surco4 = ['22.7 %', '87.1 %']
        percent_surco5 = ['22.7 %', '87.1 %']
        percent_surco6 = ['22.7 %', '87.1 %']
        percent_surco7 = ['22.7 %', '87.1 %']
        percent_surco8 = ['22.7 %', '87.1 %']
        responses = [percent_surco1, percent_surco2, percent_surco3, percent_surco4, percent_surco5, percent_surco6, percent_surco7, percent_surco8]
        global data_resultados
        data_resultados = dict()
        for row in range(len(responses)):
            for p in responses:
                data_resultados['rec'+str(row)] = dict(Parcela='Surco '+str(row),Vegetacion=p[0], Amarilleamiento= p[1])

        # Tabla:
        Table(self, data_resultados)
        
        # Labels:
        ts = datetime.now()
        totalfec = ts.strftime('%Y-%m-%d/%H:%M:%S')
        [fecha, hora] = totalfec.split('/')
        
        lb_fecinic = tk.Label(self, text="Fecha de Inicio:")  
        lb_fecinic.config(fg="#FFFFFF", bg="#766C99", font=('Helvetica', 10, 'bold'))    
        lb_fecinic.place(relx=0.5, rely=0.1)   
        fecinic = tk.Label(self, text=fecha)  
        fecinic.config(fg="#FFFFFF", bg="#F64876", font=('Helvetica', 10, 'bold'))    
        fecinic.place(relx=0.73, rely=0.1)   

        lb_horainic = tk.Label(self, text="Hora de Inicio:")  
        lb_horainic.config(fg="#FFFFFF", bg="#766C99", font=('Helvetica', 10, 'bold'))    
        lb_horainic.place(relx=0.5, rely=0.15)   
        horainic = tk.Label(self, text=hora)  
        horainic.config(fg="#FFFFFF", bg="#F64876", font=('Helvetica', 10, 'bold'))    
        horainic.place(relx=0.73, rely=0.15)   

        lb_nsurcos = tk.Label(self, text="N° de surcos analizados:")  
        lb_nsurcos.config(fg="#FFFFFF", bg="#766C99" , font=('Helvetica', 10, 'bold'))    
        lb_nsurcos.place(relx=0.5, rely=0.2) 
        nsurcos = tk.Label(self, text="8")  
        nsurcos.config(fg="#FFFFFF", bg="#F64876", font=('Helvetica', 10, 'bold'))    
        nsurcos.place(relx=0.73, rely=0.2)     

        lb_nphotosbysurco = tk.Label(self, text="N° de fotos por surco:")  
        lb_nphotosbysurco.config(fg="#FFFFFF", bg="#766C99" , font=('Helvetica', 10, 'bold'))    
        lb_nphotosbysurco.place(relx=0.5, rely=0.25)   
        nphotosbysurco = tk.Label(self, text="2")  
        nphotosbysurco.config(fg="#FFFFFF", bg="#F64876", font=('Helvetica', 10, 'bold'))    
        nphotosbysurco.place(relx=0.73, rely=0.25)     

        # Boton - Ver imagenes de Campo:
        #cultivos = ImageTk.PhotoImage(file=PATH_ICONS + 'before.png')
        btn_numeros = formato_botones(self,bd=2,width=7,height=10,bg=COLOR_BTN)
        #btn_numeros.image = cultivos
        btn_numeros.place(relx=0.05,rely=0.83)

        # Boton de Volver a Inicio:
        #casa = ImageTk.PhotoImage(file=PATH_ICONS + 'home.png')
        btn_home = formato_botones(self,bd=2,width=7,height=10,bg=COLOR_BTN_HOME,command=lambda:self.controller.show_frame(P2_Home))
        #btn_home.image = casa
        btn_home.activebackground = COLOR_ACTIVE_BTN
        btn_home.activeforeground = "#FFFFFF"
        btn_home.place(relx=0.45,rely=0.83)

        # Boton de Guardar Resultados:
        #guardar = ImageTk.PhotoImage(file=PATH_ICONS + 'icon_guardar.png')
        btn_guardar = formato_botones(self,bd=2,width=7,height=10,bg=COLOR_BTN, command=lambda:self.controller.show_frame(P7_SaveResponse))

        #btn_guardar.image = guardar
        btn_guardar.place(relx=0.85,rely=0.83)

        # Evento - Boton GPIO:
        self.after(100, self.event_real_button)

    def event_real_button(self):
        global name_frame
        
        if name_frame == 'P6_ResultsNumber':

            if not GPIO.input(PIN_PushBtn_Before):
                self.controller.show_frame(P5_ResultsImage)
                
            if not GPIO.input(PIN_PushBtn_Center):
                self.controller.show_frame(P2_Home)
                
        self.after(100, self.event_real_button)


class P7_SaveResponse(BaseFrame):
    def create_widgets(self):

        # Imagenes de guardar:
        self.btn_webserver = formato_botones(self,bd=2,width=7,height=10,bg=COLOR_BTN, text='Web')
        self.btn_local = formato_botones(self,bd=2,width=7,height=10,bg=COLOR_BTN, text='Local')
        self.btn_usb = formato_botones(self,bd=2,width=7,height=10,bg=COLOR_BTN, text='USB')

        # Boton - Volver:
        btn_volver = formato_botones(self,bd=2,width=7,height=10,bg=COLOR_BTN, text='Volver', command=lambda:self.controller.show_frame(P6_ResultsNumber))
        btn_volver.place(relx=0.05,rely=0.83)

        # Boton - Seleccionar:
        btn_select = formato_botones(self,bd=2,width=7,height=10,bg=COLOR_BTN_HOME, text='Seleccionar', command=lambda:self.seleccionar())
        btn_select.activebackground = COLOR_ACTIVE_BTN
        btn_select.activeforeground = "#FFFFFF"
        btn_select.place(relx=0.45,rely=0.83)

        # Boton - Buscar:
        btn_buscar = formato_botones(self,bd=2,width=7,height=10,bg=COLOR_BTN_HOME, text='<>', command=lambda:self.buscar_lugar())
        btn_buscar.activebackground = COLOR_ACTIVE_BTN
        btn_buscar.activeforeground = "#FFFFFF"
        btn_buscar.place(relx=0.85,rely=0.83)

    def buscar_lugar(self):
        global n_saver
        if n_saver == 1: 
            self.btn_webserver.place(relx=0.5, rely=0.15)
            self.btn_local.place_forget()
            self.btn_usb.place_forget()
        if n_saver == 2:
            self.btn_webserver.place_forget()
            self.btn_local.place(relx=0.5, rely=0.15)
            self.btn_usb.place_forget()
        if n_saver == 3:
            self.btn_webserver.place_forget()
            self.btn_local.place_forget()
            self.btn_usb.place(relx=0.5, rely=0.15)

        n_saver = n_saver + 1 
        if n_saver >= 4:
            n_saver = 1
    
    def seleccionar(self):
        global PATH_EXPORTS
        global n_saver
        global data_resultados
        if n_saver-1 == 1:
            print('guardando en web servewr ..')
        if n_saver-1 == 2:
            print('guardando en local .csv ..')
            ts = datetime.now()
            totalfec = ts.strftime('%Y-%m-%d___%H-%M-%S')
            dataCSV = [data_resultados[key] for key in data_resultados.keys()]
            headers = ['Parcela', 'Vegetacion', 'Amarilleamiento']
            data = []
            for row in dataCSV:
                data.append((row['Parcela'], row['Vegetacion'], row['Amarilleamiento']))
            data = tablib.Dataset(*data, headers=headers)
            open('exports/Analisis___'+totalfec+'.xls', 'wb').write(data.xls)

        if n_saver-1 == 0:
            print('guardando en usb ..')

    def event_real_button(self):
        global name_frame
        
        if name_frame == 'P6_ResultsNumber':

            if not GPIO.input(PIN_PushBtn_Before):
                self.controller.show_frame(P6_ResultsNumber)
                
            #if not GPIO.input(PIN_PushBtn_Center):
            #    self.controller.show_frame(P2_Home)
                
        self.after(100, self.event_real_button)

"""
Class: Run
Tipo: Ejecucion Principal  
To: python 
"""  
if __name__ == '__main__':   
    ks = GuideTk()   
    ks.mainloop()       
   




