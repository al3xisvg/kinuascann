#! /usr/bin/python     
# -*- coding: utf-8 -*-

import Tkinter as tk
import RPi.GPIO as GPIO  
import commands
import time 

"""
Class: Variables
Tipo: Declaracion
To: system
"""
PATH = commands.getoutput('pwd') + '/'
PATH_ICONS = PATH + 'icons/'

"""
Clase: Variables
Tipo: Declaracion
To: tkinter
"""
root = None
FrameSizeX = 720
FrameSizeY = 470
COLOR_FRAME = "#766C99"
COLOR_BTN = "#7DC0AE"
COLOR_ACTIVE_BTN = "#3C5181"
canvas_width = 50
canvas_height = 50
radius = 15

"""
Clase: Variables
Tipo: Declaracion
To: RPi.GPIO
"""
PIN_PushBtn = 4
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN_PushBtn, GPIO.IN)

class formato_botones(tk.Frame):
    def __init__(self, master=None, **kwargs):
        tk.Frame.__init__(self, master)
        self.rowconfigure(0, minsize=kwargs.pop('height',None))
        self.columnconfigure(0, minsize=kwargs.pop('width',None))
        self.btn = tk.Button(self, **kwargs)
        self.btn.grid(row=0, column=0, sticky="nsew")
        self.config = self.btn.config

class PythonGUI(tk.Tk):
    def __init__(self):

        # Definicion del Root:
        tk.Tk.__init__(self)
        self.title("KinuaScann 2017 v1.0")
        global root
        root = self

        # Ubicacion y Tamaño de la Ventana:
        global FrameSizeX, FrameSizeY
        ScreenSizeX = self.winfo_screenwidth()
        ScreenSizeY = self.winfo_screenheight()
        ScreenRatio = 0.8
        FramePosX = (ScreenSizeX - FrameSizeX)/2
        FramePosY = (ScreenSizeY - FrameSizeY)/2
        self.geometry("%dx%d+%d+%d" % (FrameSizeX,FrameSizeY,FramePosX,FramePosY))

        # Creacionde Widgets:
        self.create_widgets()

    def create_widgets(self):
        # Contenedor de los Frames:
        self.container = tk.Frame(self)        

        # Ubicacion del Contenedor:       
        self.container.grid(row=0, column=0, sticky=tk.W + tk.E)    

        # Generador de Frames:
        self.frames = {}
        self.pages = (P1_Presentation, P2_Home)   
        for f in self.pages:
            frame = f(self.container, self.pages)
            self.frames[f] = frame
        self.show_frame(P1_Presentation)

    def show_frame(self, page_name):
        frame = self.frames[page_name]
        frame.tkraise() 

class BaseFrame(tk.Frame):
    def __init__(self, master, controller):   
        # Definicion del Frame: 
        tk.Frame.__init__(self, master)
        self.master = master
        self.controller = controller 

        # Ubicacion del Frame:
        self.grid(row=0, column=0, sticky=tk.W + tk.E)                   

        # Tamaño y color del Frame: 
        self.config(width=FrameSizeX, height=FrameSizeY, bg="#766C99")    

        # Evitar que cambie de tamaño en cada cambio de Frame:
        self.grid_propagate(False)

        # Creacion de Widgets:
        self.create_widgets()

    def create_widgets(self):
        raise NotImplementedError 

# ################### PAGINAS DEL GUIDE ####################
class P1_Presentation(BaseFrame):
    def create_widgets(self):
        # Mensaje Introductorio:
        img = tk.PhotoImage(file=PATH_ICONS + "logoupc.png") 
        logo = tk.Label(self,image=img,bg="#766C99")
        logo.image = img
        logo.place(relx=0.45,rely=0.15)

        titulo = tk.Label(self,text="Análisis de Mildiu")
        titulo.config(fg="#FFFFFF",bg="#766C99",font=("Helvetica",50,"bold"))
        titulo.place(relx=0.11,rely=0.35) 

        texto = tk.Label(self,text="\nAlexis Vásquez García\nGian Carlos Oré Huacles\n\n\n\n@Copyright - Derechos Reservados para los creadores del Prototipo. [UPC - 2017]")  
        texto.config(fg="#FFFFFF",bg="#766C99",font=("Helvetica",10,"bold"))
        texto.place(relx=0.15,rely=0.55)

        # Canvas:
        self.canvas = tk.Canvas(self, width=canvas_width, height=canvas_height)   
        self.canvas.config(bg=COLOR_FRAME, highlightbackground=COLOR_FRAME)   
        x0 = (canvas_width/2) - radius   
        y0 = (canvas_height/2) - radius  
        x1 = (canvas_width/2) + radius   
        y1 = (canvas_height/2) + radius     
        self.circle = self.canvas.create_oval(x0, y0, x1, y1, width=2, fill="black")   
        self.canvas.place(relx=0.93, rely=0.0)  

        # Evento - Boton GPIO:   
        self.after(10, self.event_real_button)

    def event_real_button(self):   
        if GPIO.input(PIN_PushBtn):   
            self.canvas.itemconfig(self.circle, fill="black")  
        else:
            self.canvas.itemconfig(self.circle, fill="red")  
            lambda: self.controller.show_frame(P2_Home)   
        self.after(10, self.event_real_button)

class P2_Home(BaseFrame):
    def create_widgets(self):
        
        # Botón - Nuevo Análisis:
        btn_new = formato_botones(self,text="Nuevo\nAnálisis",bd=10,width=30,height=10,bg="#7DC0AE",font=("Comic Sans MS","30","bold"),activebackground="#3C5181",activeforeground="#FFFFFF",command=lambda:self.controller.show_frame(P2_1_AnalysisPage))
        btn_new.place(relx=0.1,rely=0.07)       

        # Imagen - Nuevo Análisis:
        obj_new = tk.PhotoImage(file=PATH_ICONS + "objet.png")
        img_new = tk.Label(self,image=obj_new,bg="#766C99")
        img_new.image = obj_new
        img_new.place(relx=0.1,rely=0.55)

        # Botón - Cerrar Todo:
        global root
        btn_close = formato_botones(self,text="Salir",bd=2,width=7,height=10,bg="#F64876",font=("Helvetica","20"),activebackground="#3C5181",activeforeground="#FFFFFF",command=lambda:root.destroy())    
        btn_close.place(relx=0.45,rely=0.6)

        # Botón - Configurar Iluminacion:
        btn_hist = formato_botones(self,text="Configurar\nIluminación",bd=10,width=30,height=10,bg="#7DC0AE",font=("Comic Sans MS","30","bold"),activebackground="#3C5181",activeforeground="#FFFFFF",command=lambda:self.controller.show_frame(P2_2_IluminationPage))   
        btn_hist.place(relx=0.6,rely=0.07)

        # Imagen - Configurar Iluminacion:
        obj_ilum = tk.PhotoImage(file=PATH_ICONS + "iluminacion.png")
        img_ilum = tk.Label(self,image=obj_ilum,bg="#766C99")
        img_ilum.image = obj_ilum
        img_ilum.place(relx=0.65,rely=0.45) 

# ############################ EJECUCION PRINCIPAL ########################    
if __name__ == "__main__":
    app = PythonGUI()
    app.mainloop()


