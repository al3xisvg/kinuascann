import cv2
import numpy as np
from PIL import Image
from scipy import ndimage
from skimage import color
from skimage.measure import regionprops

def etiquetado_RGB(I,ip): #entra y sale en opencv sale rgb
    M,N,P = I.shape
    I = Image.fromarray(I)
    r,g,b = I.split()
    r = np.asarray(r,dtype=np.uint8)
    g = np.asarray(g,dtype=np.uint8)
    b = np.asarray(b,dtype=np.uint8)

    rr = np.zeros((M,N))
    gg = np.zeros((M,N))
    bb = np.zeros((M,N))

    rr[ip] = r[ip]
    gg[ip] = g[ip]
    bb[ip] = b[ip]
    rr = np.asarray(rr,dtype=np.uint8)
    gg = np.asarray(gg,dtype=np.uint8)
    bb = np.asarray(bb,dtype=np.uint8)

    salidaa = Image.merge('RGB',(Image.fromarray(rr),Image.fromarray(gg),Image.fromarray(bb)))
    return  np.array(salidaa)	


def procesamiento_vegetacion(ruta_entrada, ruta_salida):
	
	
    I = cv2.imread(ruta_entrada)
    I = cv2.cvtColor(I,cv2.COLOR_BGR2RGB)	
#   IMG2 = Image.fromarray(I)         # transformacion para escalar la imagen	
#   IMG2 = IMG2.resize((1500,1200))   # tamano de la imagen a procesar
#   I    = np.array(IMG2)  			# vuelve a numpy	
    I = cv2.resize(I,(1000,750))
    M,N,P = I.shape					# datos de tama;o de la imagen		
    
	# LAB
    lab_ski = color.rgb2lab(I)	# misma entrada que para el opencv	
    l = lab_ski[:,:,0]
    a = lab_ski[:,:,1]	
    b = lab_ski[:,:,2]

#########################################   FIND     (salida ip)       ###########################################
## verde
#    ip1 = np.nonzero( (b - 0.978*a) > 39.45)
#    bin1 = np.zeros((M,N))
#    bin1[ip1] = True
    bin1 = (b - 1.978*a) > 19.45
    ip = np.nonzero( bin1 == True)   
    

######## BINARIZACION     #############################################
    bina = np.zeros((M,N))
    bina[ip] = 255
######## FILTRADO #####################################
    salida_filtro = ndimage.median_filter(bina, 7)
    ipf = np.nonzero(salida_filtro > 90)

#salida_filtro = etiquetado_RGB(I,ipf)  # para mostrar en pantalla
##### ETIQUETADO  ###############################################
    mask = np.zeros((M,N))
    mask[ipf] = 1
	
# Marker labelling
    #_, markers = cv2.connectedComponents(np.uint8(mask))
    #numero_etiquetas = np.amax(markers)
    markers, numero_etiquetas = ndimage.label(mask)		
    sizes = ndimage.sum(mask, markers, range(numero_etiquetas + 1))
    mask_size = sizes < 8000
    remove_pixel = mask_size[markers]
    markers[remove_pixel] = 0

    ip = np.nonzero(markers > 0)
    img = etiquetado_RGB(I,ip)  # en opencv 
    img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR)
    cv2.imwrite(ruta_salida,img)

def procesamiento_amarillo(ruta_entrada, ruta_salida):		
    I = cv2.imread(ruta_entrada)
    I = cv2.cvtColor(I,cv2.COLOR_BGR2RGB)	
#    IMG = Image.fromarray(I)         	# transformacion para escalar la imagen	
#    IMG = IMG.resize((2592/2,1944/2))   # tamano de la imagen a procesar
#    I    = np.array(IMG)  				# vuelve a numpy
    
    M,N,P = I.shape						# datos de tama;o de la imagen		
	# LAB
    lab_ski = color.rgb2lab(I)	# misma entrada que para el opencv	
    l = lab_ski[:,:,0]
    a = lab_ski[:,:,1]	
    b = lab_ski[:,:,2]
	
	
	
#########################################   FIND     (salida ip)       ###########################################
### amarillo 1ab

    bin1 = (0.557*a + b) > 19
    bin2 = a > -14
    bin3 = a > -15
    bin4 = a < 0
    bin5 = b > 56
    bin6 = b < 68
	
    resul1 = np.logical_and(bin1,bin2)
    resul2 = np.logical_and(bin3,bin4)
    resul3 = np.logical_and(bin5,bin6)
    resul4 = np.logical_and(resul2,resul3)   
    resul5 = np.logical_not(resul4)
    resulx1 = np.logical_and(resul1,resul5)
    
    

### amarillo RGB 
    r = I[:,:,0]
    g = I[:,:,1]
    b = I[:,:,2]

    bin7 = r > (g + b)
    bin8 = r > 100
    bin9 = g > 100
    bin10= b < 120
	
    resul6    = np.logical_and(bin8,bin9)
    resul7    = np.logical_and(resul6,bin10)
    resulx2 = np.logical_or(bin7,resul7)
    
    

    resul_amarillo = np.logical_and(resulx1,resulx2)
    ip = np.nonzero(resul_amarillo == True)
	
######## BINARIZACION     #############################################
    bina = np.zeros((M,N))
    bina[ip] = 255
######## FILTRADO #####################################
    salida_filtro = ndimage.median_filter(bina, 3)
    ipf = np.nonzero(salida_filtro > 90)

##### ETIQUETADO  ###############################################
    mask = np.zeros((M,N))
    mask[ipf] = 1
	
# Marker labelling
    label_im, nb_labels = ndimage.label(mask)
    props = regionprops(label_im)
    sizes = ndimage.sum(mask, label_im, range(nb_labels + 1))	
    excentricidad = np.zeros((nb_labels+1))
    excentricidad[0] = 1	
    i = 0
    while i < nb_labels:
        excentricidad[i+1] = props[i].eccentricity
        i = i + 1
		
    mask_size = sizes < 20
    mask_exc = excentricidad > 0.97
    total_bool = np.logical_or(mask_size,mask_exc)
    remove_pixel = total_bool[label_im]
    label_im[remove_pixel] = 0

    ip = np.nonzero(label_im > 0)
    img = etiquetado_RGB(I,ip)  # en opencv 
    img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR)
    cv2.imwrite(ruta_salida,img)