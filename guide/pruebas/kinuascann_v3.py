#! /usr/bin/python2.7   
# -*- coding: utf-8 -*-   

import Tkinter as tk  
import RPi.GPIO as GPIO   
import commands  
import time  

"""
Class: Variables   
Tipo: Declaracion  
To: system   
"""  
PATH = commands.getoutput('pwd') + '/'   
PATH_ICONS = PATH + 'icons/'    

"""
Clase: Variables  
Tipo: Declaracion  
To: tkinter  
"""
root = None  
FrameSizeX = 720   
FrameSizeY = 470   
COLOR_FRAME = "#766C99"  
COLOR_BTN = "#7DC0AE"   
COLOR_ACTIVE_BTN = "#3C5181"  
canvas_width = 50   
canvas_height = 50  
radius = 15    

"""
Clase: Variables  
Tipo: Declaracion  
To: RPi.GPIO     
"""
PIN_PushBtn = 4  
GPIO.setmode(GPIO.BCM)    
GPIO.setup(PIN_PushBtn, GPIO.IN)    

class formato_botones(tk.Frame):
    def __init__(self, master=None, **kwargs):
        tk.Frame.__init__(self, master)
        self.rowconfigure(0, minsize=kwargs.pop('height',None))
        self.columnconfigure(0, minsize=kwargs.pop('width',None))
        self.btn = tk.Button(self, **kwargs)
        self.btn.grid(row=0, column=0, sticky="nsew")
        self.config = self.btn.config

class GuideTk(tk.Tk):    
    def __init__(self, *args, **kwargs):   

        # Definicion - Tk() Object:  
        tk.Tk.__init__(self)   
        self.title('KinuaScann 2017 v2.0')     

        # Root:  
        global root  
        root = self 

        # Ubicacion y Tamaño del Tk() Object:  
        ScreenSizeX = self.winfo_screenwidth()   
        ScreenSizeY = self.winfo_screenheight() 
        ScreenRatio = 0.8   
        FramePosX = (ScreenSizeX - FrameSizeX)/2   
        FramePosY = (ScreenSizeY - FrameSizeY)/2    
        self.geometry('%dx%d+%d+%d' % (FrameSizeX,FrameSizeY,FramePosX,FramePosY))      

        # Creacion de Componentes:  
        self.create_widgets()   

    def create_widgets(self):    

        # Contenedor Tk():  
        self.container = tk.Frame(root)   
        self.container.grid(row=0, column=0, sticky=tk.W + tk.E)  

        # Inicializacion de Ventanas:   
        self.frames = {}    
        self.pages = (P1_Presentation, P2_Home)   
        for F in self.pages:   
            frame = F(self.container, self, self.pages)    
            self.frames[F] = frame  
        
        # Visualizacion inicial:
        self.show_frame(P1_Presentation)   

    def show_frame(self, page_name):    
        frame = self.frames[page_name]    
        frame.tkraise()   

class BaseFrame(tk.Frame):
    def __init__(self, parent, controller, pages):   

        # Definicion - Frame() Object:      
        tk.Frame.__init__(self, parent)   
        self.parent = parent   
        self.controller = controller  
        self.pages = pages   

        # Caracteristicas de Frame:  
        self.grid(row=0, column=0, sticky=tk.W + tk.E)   
        self.config(width=720, height=470,bg=COLOR_FRAME)    
        self.grid_propagate(False)       

        # Crear componentes:    
        self.create_widgets()   

    def create_widgets(self):    
        raise NotImplementedError  

class P1_Presentation(BaseFrame):   
    def create_widgets(self):    

        # Mensaje Introductorio:   
        img = tk.PhotoImage(file=PATH_ICONS + 'logoupc.png')   
        logo = tk.Label(self, image=img, bg=COLOR_FRAME)    
        logo.image = img   
        logo.place(relx=0.45, rely=0.15)   

        titulo = tk.Label(self, text='Análisis de Mildiu')   
        titulo.config(fg="#FFFFFF", bg=COLOR_FRAME, font=('Helvetica', 50, 'bold'))   
        titulo.place(relx=0.11, rely=0.35)   

        texto = tk.Label(self, text="\nAlexis Vásquez García\nGian Carlos Oré Huacles\n\n\n\n@Copyright - Derechos Reservados para los creadores del Prototipo. [UPC - 2019]")  
        texto.config(fg="#FFFFFF", bg=COLOR_FRAME, font=('Helvetica', 10, 'bold'))    
        texto.place(relx=0.15, rely=0.55)   

        # Canvas:
        self.canvas = tk.Canvas(self, width=canvas_width, height=canvas_height)   
        self.canvas.config(bg=COLOR_FRAME, highlightbackground=COLOR_FRAME)   
        x0 = (canvas_width/2) - radius   
        y0 = (canvas_height/2) - radius  
        x1 = (canvas_width/2) + radius   
        y1 = (canvas_height/2) + radius     
        self.circle = self.canvas.create_oval(x0, y0, x1, y1, width=2, fill="black")   
        self.canvas.place(relx=0.93, rely=0.0)  

        # Evento - Boton GPIO:   
        self.after(100, self.event_real_button)   

    def event_real_button(self):   
        if GPIO.input(PIN_PushBtn):   
            self.canvas.itemconfig(self.circle, fill="black")  
        else:
            self.canvas.itemconfig(self.circle, fill="red")  
            self.controller.show_frame(P2_Home)   
        self.after(100, self.event_real_button)   

class P2_Home(BaseFrame):    
    def create_widgets(self):   

        # Botón - Nuevo Análisis:
        btn_new = formato_botones(self, text="Nuevo\nAnálisis",bd=10,width=30,height=10,bg=COLOR_BTN,font=("Comic Sans MS","30","bold"),activebackground=COLOR_ACTIVE_BTN,activeforeground="#FFFFFF")
        btn_new.place(relx=0.1,rely=0.07)       

        # Imagen - Nuevo Análisis:
        obj_new = tk.PhotoImage(file=PATH_ICONS + "objet.png")
        img_new = tk.Label(self,image=obj_new,bg=COLOR_FRAME)
        img_new.image = obj_new
        img_new.place(relx=0.1,rely=0.55)

        # Botón - Cerrar Todo:
        global root 
        btn_close = formato_botones(self,text="Salir",bd=2,width=7,height=10,bg="#F64876",font=("Helvetica","20"),activebackground=COLOR_ACTIVE_BTN,activeforeground="#FFFFFF",command=lambda:root.destroy())    
        btn_close.place(relx=0.45,rely=0.6)

        # Botón - Configurar Iluminacion:
        btn_hist = formato_botones(self,text="Configurar\nIluminación",bd=10,width=30,height=10,bg=COLOR_BTN,font=("Comic Sans MS","30","bold"),activebackground=COLOR_ACTIVE_BTN,activeforeground="#FFFFFF")   
        btn_hist.place(relx=0.6,rely=0.07)

        # Imagen - Configurar Iluminacion:
        obj_ilum = tk.PhotoImage(file=PATH_ICONS + "iluminacion.png")
        img_ilum = tk.Label(self,image=obj_ilum,bg=COLOR_FRAME)
        img_ilum.image = obj_ilum
        img_ilum.place(relx=0.65,rely=0.45)

"""
Class: Run
Tipo: Ejecucion Principal  
To: python 
"""  
if __name__ == '__main__':   
    ks = GuideTk()   
    ks.mainloop()       
   




