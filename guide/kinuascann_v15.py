#! /usr/bin/python2.7   
# -*- coding: utf-8 -*-   

import Tkinter as tk  
import RPi.GPIO as GPIO   
import commands
import cv2
import os
import logging
from PIL import Image, ImageTk
from datetime import datetime
from fcn_procesamiento import procesamiento_vegetacion
from fcn_procesamiento import procesamiento_amarillo

"""
Class: Variables   
Tipo: Declaracion  
To: system   
"""  
PATH = commands.getoutput('pwd') + '/'   
PATH_ICONS = PATH + 'icons/'
PATH_CAPTURES = PATH + 'captures/'
PATH_TEMPS = PATH + 'temps/'
ruta_img_orig = PATH_TEMPS + 'original.png'
ruta_img_veget = PATH_TEMPS + 'r1_vegetacion.png'
ruta_img_amari = PATH_TEMPS + 'r2_amarilleamiento.png'

"""
Clase: Variables  
Tipo: Declaracion  
To: tkinter  
"""
root = None  
FrameSizeX = 720   
FrameSizeY = 470   
COLOR_FRAME = "#766C99"  
COLOR_BTN = "#7DC0AE"   
COLOR_ACTIVE_BTN = "#3C5181"
COLOR_BTN_HOME = "#F64876"
canvas_width = 50   
canvas_height = 50  
radius = 15    

"""
Clase: Variables  
Tipo: Declaracion  
To: RPi.GPIO     
"""
PIN_PushBtn_Before = 3
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN_PushBtn_Before, GPIO.IN)

PIN_PushBtn_Center = 2
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN_PushBtn_Center, GPIO.IN) 

PIN_PushBtn_Next = 4  
GPIO.setmode(GPIO.BCM)    
GPIO.setup(PIN_PushBtn_Next, GPIO.IN)    

"""
Clase: Flags
Tipo: Booleanos
To: Frames 
"""
name_frame = None 

"""
Clase: PiCamera
Tipo: Driver
To: Videocaptura
"""
commands.getoutput('sudo modprobe bcm2835-v4l2')

"""
Clase: Flags
Tipo: Booleanos
To: Camara
"""
flag_captura = False
flag_procesar = True

"""
Clase: Config
Tipo: Sistema
To: Logs
"""
formatter = logging.Formatter("%(asctime)s %(levelname)s %(message)s","%Y-%m-%d %H:%M:%S")
logger = logging.getLogger()
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
ch.setFormatter(formatter)
logger.addHandler(ch)

class formato_botones(tk.Frame):
    def __init__(self, master=None, **kwargs):
        tk.Frame.__init__(self, master)
        self.rowconfigure(0, minsize=kwargs.pop('height',None))
        self.columnconfigure(0, minsize=kwargs.pop('width',None))
        self.btn = tk.Button(self, **kwargs)
        self.btn.grid(row=0, column=0, sticky="nsew")
        self.config = self.btn.config

class GuideTk(tk.Tk):    
    def __init__(self, *args, **kwargs):   

        # Definicion - Tk() Object:  
        tk.Tk.__init__(self)   
        self.title('KinuaScann 2017 v2.0')     

        # Root:  
        global root  
        root = self 

        # Ubicacion y Tamaño del Tk() Object:  
        ScreenSizeX = self.winfo_screenwidth()   
        ScreenSizeY = self.winfo_screenheight() 
        ScreenRatio = 0.8   
        FramePosX = (ScreenSizeX - FrameSizeX)/2   
        FramePosY = (ScreenSizeY - FrameSizeY)/2    
        self.geometry('%dx%d+%d+%d' % (FrameSizeX,FrameSizeY,FramePosX,FramePosY))      

        # Creacion de Componentes:  
        self.create_widgets()   

    def create_widgets(self):    

        # Contenedor Tk():  
        self.container = tk.Frame(root)   
        self.container.grid(row=0, column=0, sticky=tk.W + tk.E)  

        # Inicializacion de Ventanas:   
        self.frames = {}    
        self.pages = (P1_Presentation, P2_Home, P3_Analysis, P4_Processing, P5_ResultsImage, P6_ResultsNumber)   
        for F in self.pages:   
            frame = F(self.container, self, self.pages)    
            self.frames[F] = frame  
        
        # Visualizacion inicial:
        logger.info('[SYSTEM-START] Bienvenido a KinuaScann')
        self.show_frame(P1_Presentation)

    def show_frame(self, page_name):
        global name_frame
        name_frame = (str(page_name)).replace('__main__.','')
        logger.info('[WINDOW] Pass to Frame ' + str(name_frame))
        frame = self.frames[page_name]    
        frame.tkraise()   

class BaseFrame(tk.Frame):
    def __init__(self, parent, controller, pages):   

        # Definicion - Frame() Object:      
        tk.Frame.__init__(self, parent)   
        self.parent = parent   
        self.controller = controller  
        self.pages = pages   

        # Caracteristicas de Frame:  
        self.grid(row=0, column=0, sticky=tk.W + tk.E)   
        self.config(width=720, height=470,bg=COLOR_FRAME)    
        self.grid_propagate(False)       

        # Crear componentes:    
        self.create_widgets()   

        # Canvas:
        self.canvas = tk.Canvas(self, width=canvas_width, height=canvas_height)
        self.canvas.config(bg=COLOR_FRAME, highlightbackground=COLOR_FRAME)
        x0 = (canvas_width/2) - radius
        y0 = (canvas_height/2) - radius
        x1 = (canvas_width/2) + radius
        y1 = (canvas_height/2) + radius
        self.circle = self.canvas.create_oval(x0, y0, x1, y1, width=2, fill="black")
        self.canvas.place(relx=0.93, rely=0.0)

    def create_widgets(self):    
        raise NotImplementedError  

class P1_Presentation(BaseFrame):   
    def create_widgets(self):    

        # Mensaje Introductorio:   
        img = tk.PhotoImage(file=PATH_ICONS + 'logoupc.png')   
        logo = tk.Label(self, image=img, bg=COLOR_FRAME)    
        logo.image = img   
        logo.place(relx=0.45, rely=0.15)   

        titulo = tk.Label(self, text='Análisis de Mildiu')   
        titulo.config(fg="#FFFFFF", bg=COLOR_FRAME, font=('Helvetica', 50, 'bold'))   
        titulo.place(relx=0.11, rely=0.35)   

        texto = tk.Label(self, text="\nAlexis Vásquez García\nGian Carlos Oré Huacles\n\n\n\n@Copyright - Derechos Reservados para los creadores del Prototipo. [UPC - 2019]")  
        texto.config(fg="#FFFFFF", bg=COLOR_FRAME, font=('Helvetica', 10, 'bold'))    
        texto.place(relx=0.15, rely=0.55)   

        # Evento - Boton GPIO:   
        self.after(100, self.event_real_button)   

    def event_real_button(self):
        global name_frame

        if name_frame == 'P1_Presentation':
            if not GPIO.input(PIN_PushBtn_Center):   
                self.controller.show_frame(P2_Home)
                
        self.after(100, self.event_real_button)   

class P2_Home(BaseFrame):    
    def create_widgets(self):   

        # Botón - Configurar Iluminacion:
        btn_hist = formato_botones(self,text="Configurar\nIluminación",bd=10,width=30,height=10,bg=COLOR_BTN,font=("Helvetica","25","bold"),activebackground=COLOR_ACTIVE_BTN,activeforeground="#FFFFFF")   
        btn_hist.place(relx=0.1,rely=0.07)

        # Imagen - Configurar Iluminacion:
        obj_ilum = tk.PhotoImage(file=PATH_ICONS + "iluminacion.png")
        img_ilum = tk.Label(self,image=obj_ilum,bg=COLOR_FRAME)
        img_ilum.image = obj_ilum
        img_ilum.place(relx=0.15,rely=0.45)

        # Botón - Cerrar Todo:
        global root 
        btn_close = formato_botones(self,text="Salir",bd=2,width=7,height=10,bg=COLOR_BTN_HOME,font=("Helvetica","20"),activebackground=COLOR_ACTIVE_BTN,activeforeground="#FFFFFF",command=lambda:root.destroy())    
        btn_close.place(relx=0.45,rely=0.6)

        # Botón - Nuevo Análisis:
        btn_new = formato_botones(self, text="Nuevo\nAnálisis",bd=10,width=30,height=10,bg=COLOR_BTN,font=("Helvetica","25","bold"),activebackground=COLOR_ACTIVE_BTN,activeforeground="#FFFFFF", command=lambda: self.controller.show_frame(P3_Analysis))
        btn_new.place(relx=0.65,rely=0.07)

        # Imagen - Nuevo Análisis:
        obj_new = tk.PhotoImage(file=PATH_ICONS + "objet.png")
        img_new = tk.Label(self,image=obj_new,bg=COLOR_FRAME)
        img_new.image = obj_new
        img_new.place(relx=0.65,rely=0.5)

        # Evento - Boton GPIO:
        self.after(100, self.event_real_button)

    def event_real_button(self):
        global name_frame
        global root

        if name_frame == 'P2_Home':
            
            if not GPIO.input(PIN_PushBtn_Before):            
                #self.controller.show_frame(P1_Presentation)
                print 'Falta implementar conf. ilum'

            if not GPIO.input(PIN_PushBtn_Center):
                logger.info('[SYSTEM-END] Hasta pronto.')
                root.destroy()

            if not GPIO.input(PIN_PushBtn_Next):
                self.controller.show_frame(P3_Analysis)
                
        self.after(100, self.event_real_button)

class P3_Analysis(BaseFrame):    
    def create_widgets(self):   

        # Creacion de Camara:
        width, height = 650, 350
        self.vs = cv2.VideoCapture(0)
        self.vs.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, width)
        self.vs.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, height) 

        # Boton de Captura:
        self.foto = tk.PhotoImage(file=PATH_ICONS + 'camara.png')
        self.refoto = tk.PhotoImage(file=PATH_ICONS + 'again.png')
        self.btn_capture = tk.Button(self,image=self.foto,bd=10,bg=COLOR_BTN,activebackground=COLOR_ACTIVE_BTN,activeforeground="#FFFFFF",command=lambda:self.capturar)
        self.btn_capture.image = self.foto
        self.btn_capture.place(relx=0.05,rely=0.82)

        # Boton de Volver a Inicio:
        casa = tk.PhotoImage(file=PATH_ICONS + 'home.png')
        btn_home = formato_botones(self,image=casa,bd=2,width=7,height=10,bg=COLOR_BTN_HOME,command=lambda:self.controller.show_frame(P2_Home))
        btn_home.image = casa
        btn_home.activebackground = COLOR_ACTIVE_BTN
        btn_home.activeforeground = "#FFFFFF"
        btn_home.place(relx=0.45,rely=0.84)

        # Boton de Procesar:
        proceso = tk.PhotoImage(file=PATH_ICONS + 'procesar.png')
        self.btn_procesar = tk.Button(self,image=proceso,bd=10,state='disabled',bg=COLOR_BTN,activebackground=COLOR_ACTIVE_BTN,activeforeground="#FFFFFF",command=lambda:[self.current_image.save(self.p,'PNG'),self.current_image.save(self.q,'PNG'),self.controller.show_frame(P4_Processing)])
        self.btn_procesar.image = proceso
        self.btn_procesar.place(relx=0.83,rely=0.82)

        # Panel de Visualizacion:
        self.panel = tk.Label(self)
        self.panel.place(relx=0.05,rely=0.05)

        # Start Video Stream:
        self.after(30, self.video_loop)

        # Evento - Boton GPIO:
        self.after(100, self.event_real_button)

    def capturar(self): 
        global flag_captura
        flag_captura = not flag_captura

        ts = datetime.now()
        filename = '{}.png'.format(ts.strftime('%Y-%m-%d___%H-%M-%S'))
        self.p = os.path.join(PATH_CAPTURES,filename)
        self.q = os.path.join(PATH_TEMPS,'original.png')
        
        if flag_captura is True:
            self.btn_capture.config(image=self.refoto)
            self.btn_procesar.config(state='normal')
        else:
            self.btn_capture.config(image=self.foto)
            self.btn_procesar.config(state='disabled')

            self.after(30, self.video_loop)

    def video_loop(self):
        global flag_captura
        
        ok, frame = self.vs.read()
        cv2image = cv2.cvtColor(frame,cv2.COLOR_BGR2RGBA)

        self.current_image = Image.fromarray(cv2image)
        self.imgtk = ImageTk.PhotoImage(image=self.current_image)

        self.panel.imgtk = self.imgtk
        self.panel.config(image=self.imgtk)

        if flag_captura is False:
            self.after(30, self.video_loop)

    def event_real_button(self):
        global name_frame
        global flag_captura

        if name_frame == 'P3_Analysis':
            
            if not GPIO.input(PIN_PushBtn_Before):
                self.capturar()
        
            if not GPIO.input(PIN_PushBtn_Center):
                self.controller.show_frame(P2_Home)

            if not GPIO.input(PIN_PushBtn_Next):
                if flag_captura is True:

                    logger.info('[DEVICE] Capturing image and equalize ...')
                    
                    flag_captura = not flag_captura
                    self.btn_capture.config(image=self.foto)
                    self.btn_procesar.config(state='disabled')
                    self.video_loop()
                    
                    self.current_image.save(self.p,'PNG')
                    self.current_image.save(self.q,'PNG')

                    self.controller.show_frame(P4_Processing)
                
        self.after(100, self.event_real_button)

class P4_Processing(BaseFrame):
    def create_widgets(self):

        # Panel de Visualizacion:
        self.panel = tk.Label(self)
        self.panel.place(relx=0.05,rely=0.05)

        # Boton para ver Imagen Capturada:
        ojo = tk.PhotoImage(file=PATH_ICONS + 'icon_eye.png')
        btn_showCapture = formato_botones(self,image=ojo,bd=2,width=7,height=10,bg=COLOR_BTN,command=lambda:self.show_foto_capturada())
        btn_showCapture.image = ojo
        btn_showCapture.place(relx=0.05,rely=0.83)

        # Boton de Volver a Inicio:
        casa = tk.PhotoImage(file=PATH_ICONS + 'home.png')
        btn_home = formato_botones(self,image=casa,bd=2,width=7,height=10,bg=COLOR_BTN_HOME,command=lambda:self.controller.show_frame(P2_Home))
        btn_home.image = casa
        btn_home.activebackground = COLOR_ACTIVE_BTN
        btn_home.activeforeground = "#FFFFFF"
        btn_home.place(relx=0.45,rely=0.83)

        # Boton de Procesar:
        procesar = tk.PhotoImage(file=PATH_ICONS + 'procesar.png')
        btn_procesar = formato_botones(self,image=procesar,bd=2,width=7,height=10,bg=COLOR_BTN)
        btn_procesar.image = procesar
        btn_procesar.place(relx=0.85,rely=0.83)
        
        # Evento - Boton GPIO:
        self.after(100, self.event_real_button)

    def show_foto_capturada(self):
        try:
            original = Image.open(ruta_img_orig)
            im_original = ImageTk.PhotoImage(original)
            self.panel.image = im_original
            self.panel.configure(image=im_original)
        except Exception as ex:
            logger.warning('[IMAGES] No hay archivos en directorio temps/')

    def event_real_button(self):
        global name_frame
        
        if name_frame == 'P4_Processing':

            if not GPIO.input(PIN_PushBtn_Before):
                # Mostrar imagen en Panel:
                self.show_foto_capturada()
            
            if not GPIO.input(PIN_PushBtn_Center):
                # Limpiar imagen del Panel:
                self.panel.image = ''
                self.panel.config(image='')
                
                self.controller.show_frame(P2_Home)

            if not GPIO.input(PIN_PushBtn_Next):
                # Limpiar imagen del Panel:
                self.panel.image = ''
                self.panel.config(image='')
                
                time_start = datetime.now()
                logger.info('[PROCESSING] Iniciando procesamiento ...')
                logger.info('[PROCESSING] Filtrando tierra y aire de la imagen ...')
                procesamiento_vegetacion(ruta_img_orig, ruta_img_veget)
                logger.info('[PROCESSING] Filtrando hojas sanas, tallos y objetos de la imagen ...')
                procesamiento_amarillo(ruta_img_veget, ruta_img_amari)
                time_end = datetime.now()
                time_range = (time_end-time_start).total_seconds()
                logger.info('[PROCESSING] Procesamiento Finalizado!! - time: ' + str(round(time_range,2)) + ' s')
                
                self.controller.show_frame(P5_ResultsImage)
                
        self.after(100, self.event_real_button)

class P5_ResultsImage(BaseFrame):
    def create_widgets(self):

        # Panel de Visualizacion:
        self.panel = tk.Label(self)
        self.panel.place(relx=0.05,rely=0.05)

        # Boton - Ver Imagen sin Vegetacion:
        vegetacion = tk.PhotoImage(file=PATH_ICONS + 'icon_vegetacion.png')
        btn_verVegetacion = formato_botones(self,image=vegetacion,bd=2,width=7,height=10,bg=COLOR_BTN)
        btn_verVegetacion.image = vegetacion
        btn_verVegetacion.place(relx=0.05,rely=0.83)

        # Boton - Ver Imagen solo Amarilleamiento:
        amarilleamiento = tk.PhotoImage(file=PATH_ICONS + 'icon_amarilleamiento.png')
        btn_verAmarilleamiento = formato_botones(self,image=amarilleamiento,bd=2,width=7,height=10,bg=COLOR_BTN)
        btn_verAmarilleamiento.image = amarilleamiento
        btn_verAmarilleamiento.place(relx=0.45,rely=0.83)

        # Boton - Ver resultado numerico:
        porcentaje = tk.PhotoImage(file=PATH_ICONS + 'next.png')
        btn_numeros = formato_botones(self,image=porcentaje,bd=2,width=7,height=10,bg=COLOR_BTN)
        btn_numeros.image = porcentaje
        btn_numeros.place(relx=0.85,rely=0.83)

        # Evento - Boton GPIO:
        self.after(100, self.event_real_button)

    def event_real_button(self):
        global name_frame
        global flag_procesar
        
        if name_frame == 'P5_ResultsImage':

            if not GPIO.input(PIN_PushBtn_Before):
                veget = Image.open(ruta_img_veget)
                im_veget = ImageTk.PhotoImage(veget)
                self.panel.image = im_veget
                self.panel.configure(image=im_veget)
            
            if not GPIO.input(PIN_PushBtn_Center):
                amari = Image.open(ruta_img_amari)
                im_amari = ImageTk.PhotoImage(amari)
                self.panel.image = im_amari
                self.panel.configure(image=im_amari)

            if not GPIO.input(PIN_PushBtn_Next):
                self.controller.show_frame(P6_ResultsNumber)

        self.after(100, self.event_real_button)

class P6_ResultsNumber(BaseFrame):
    def create_widgets(self):

        # Tabla de Resultados:
        h = 5
        w = 3
        for i in range(h):
            for j in range(w):
                b = tk.Entry(self,text="")
                b.grid(row=i,column=j)
    
        # Boton - Ver imagenes de Campo:
        cultivos = tk.PhotoImage(file=PATH_ICONS + 'before.png')
        btn_numeros = formato_botones(self,image=cultivos,bd=2,width=7,height=10,bg=COLOR_BTN)
        btn_numeros.image = cultivos
        btn_numeros.place(relx=0.05,rely=0.83)

        # Boton de Volver a Inicio:
        casa = tk.PhotoImage(file=PATH_ICONS + 'home.png')
        btn_home = formato_botones(self,image=casa,bd=2,width=7,height=10,bg=COLOR_BTN_HOME,command=lambda:self.controller.show_frame(P2_Home))
        btn_home.image = casa
        btn_home.activebackground = COLOR_ACTIVE_BTN
        btn_home.activeforeground = "#FFFFFF"
        btn_home.place(relx=0.45,rely=0.83)

        # Boton de Guardar Resultados:
        guardar = tk.PhotoImage(file=PATH_ICONS + 'icon_guardar.png')
        btn_guardar = formato_botones(self,image=guardar,bd=2,width=7,height=10,bg=COLOR_BTN)
        btn_guardar.image = guardar
        btn_guardar.place(relx=0.85,rely=0.83)

        # Evento - Boton GPIO:
        self.after(100, self.event_real_button)

    def event_real_button(self):
        global name_frame
        
        if name_frame == 'P6_ResultsNumber':

            if not GPIO.input(PIN_PushBtn_Before):
                self.controller.show_frame(P5_ResultsImage)
                
            if not GPIO.input(PIN_PushBtn_Center):
                self.controller.show_frame(P2_Home)
                
        self.after(100, self.event_real_button)
    
"""
Class: Run
Tipo: Ejecucion Principal  
To: python 
"""  
if __name__ == '__main__':   
    ks = GuideTk()   
    ks.mainloop()       
   




